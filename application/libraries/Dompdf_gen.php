<?php class Dompdf_gen
{
    function __construct()
    {
        require_once APPPATH . 'third_party/dompdf/dompdf_config.inc.php';


        $pdf = new DOMPDF();
        $ci = &get_instance();
        $ci->dompdf = $pdf;
    }
}
