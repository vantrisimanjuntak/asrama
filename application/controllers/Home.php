<?php class Home extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Berita_model');
		$this->load->helper('url', 'string');
	}
	function index()
	{
		$data['title'] = "Home | Asrama St. Albertus Magnus Aekkanopan";
		$data['berita'] = $this->Berita_model->getAllNews();

		$this->load->view('home/index', $data);
	}
}
