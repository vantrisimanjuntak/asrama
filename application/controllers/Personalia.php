<?php if (!defined('BASEPATH'))
	exit('No direct script acces allowed');
/**
 * 
 */
class Personalia extends CI_Controller
{
	function __construction()
	{
		parent::__construct();
	}
	function index()
	{
		$data['title'] = "Personalia | Asrama St. Albertus Magnus Aekkanopan";
		$this->load->view('personalia/index', $data);
	}
}
