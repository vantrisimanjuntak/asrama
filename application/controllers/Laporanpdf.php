<?php class Laporanpdf extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    function index()
    {

        $data = array(
            'record' => $this->db->query('SELECT * FROM siswa')
        );

        $this->load->view('laporan_pdf', $data);
        // dapatkan output html
        $html = $this->output->get_output();
        // Load/panggil library dompdf nya
        $this->load->library('Dompdf_gen');
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        // Untuk menampilkan preview PDF
        $sekarang = date("d:F:Y:h:m:s");
        ob_end_clean();
        $this->dompdf->stream("Pendaftaran " . $sekarang . ".pdf", array("Attachment" => 0));
    }
}
