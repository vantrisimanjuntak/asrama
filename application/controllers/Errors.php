<?php class Errors extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }
    function index()
    {
        $data['title'] = "Not Found | Asrama St. Albertus Magnus Aek Kanopan";

        $this->load->view('404/header', $data);
        $this->load->view('navbar');
        $this->load->view('marquee');
        $this->load->view('404/content');
        $this->load->view('footer');
    }
}
