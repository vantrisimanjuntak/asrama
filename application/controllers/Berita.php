<?php

/**
 * 
 */
class Berita extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Berita_model');
		$this->load->helper('url');
	}

	function show_detail($link)
	{

		$data['title'] = "Berita | Asrama St. Albertus Magnus Aekkanopan";
		// $id = $this->input->get('id');
		$link = $this->uri->segment('3');
		$data['berita'] = $this->Berita_model->show_detail($link);
		$this->load->view('berita/index', $data);
	}
}
