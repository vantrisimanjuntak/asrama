<?php if (!defined('BASEPATH'))
	exit('No direct script acces allowed');
/**
 * 
 */
class Galeri extends CI_Controller
{
	function index()
	{
		$data['title'] = "Galeri | Asrama St. Albertus Magnus Aekkanopan";
		$this->load->view('galeri/index', $data);
	}
}
