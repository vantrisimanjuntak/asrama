	<?php 
	/**
	 * 
	 */
	class Upload extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();

		}
		function index()
		{
			$this->load->view('v_upload');
		}
		public function prosesupload()
		{
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']='1000';
			$config['max_height']='768';

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload())
			{
				echo "Gagal";
			}
			else
			{
				$img = $this->upload->data();
				$gambar = $img['file_name'];
				$nama = $this->input->post('nama', true);
				$harga = $this->input->post('harga', true);
				$data = array(
					'nama_produk' =>$nama,
					'harga_produk' =>$harga,
					'gambar' => $gambar,

				);
				$this->db->insert('tb_produk', $data);
				redirect('upload/beranda');
			}
		}
		function beranda()
		{
			$this->data['produk'] = $this->db->get('tb_produk')->result_array();
			$this->load->view('v_dashboard', $this->data);
		}
	}
?>