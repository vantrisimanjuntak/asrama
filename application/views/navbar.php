<nav class="navbar navbar-expand-sm bg-dark navbar-dark ">
  <a class="navbar-brand mx-auto" href="<?php echo base_url('') ?>">
    <img style="width: 60px; height: 50px;" src="<?php echo base_url('image/LogoBaru.png') ?>" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="<?php echo base_url('') ?>" class="nav-link">
          <i class="fa fa-home" style="color:white"></i>
        </a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a href="<?php echo base_url('personalia') ?>" class="nav-link" style="color:white; font-size: 15px;">Personalia</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item ">
        <a href="<?php echo base_url('sejarah') ?>" class="nav-link" style="color:white; font-size: 15px;">Sejarah</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a href="<?php echo base_url('galeri') ?>" class="nav-link" style="color:white; font-size: 15px;">Galeri</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a href="<?php echo base_url('hubungi_kami') ?>" class="nav-link" style="color:white; font-size: 15px;">Hubungi Kami</a>
      </li>
    </ul>
  </div>
</nav>