<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shorcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
  <!-- Bootstrap 4 CSS -->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap4/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome 4 -->
  <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Font Google -->
  <link href="https://fonts.googleapis.com/css2?family=Martel:wght@700&family=Noto+Serif&family=Raleway:ital,wght@1,100&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Amethysta&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Alice&family=Caladea:ital@1&display=swap" rel="stylesheet">
  <style>
    .carousel-item {
      height: 100vhmin;
      min-height: 550px;
      background: no-repeat center center scroll;

      background-size: scroll;
      background-origin: content-box;
      -webkit-background-size: contain;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;


    }

    #small.carousel-item {


      height: 450px;
      background-origin: content-box;

      background: center no-repeat;
    }
  </style>
  <title><?= $title; ?></title>
</head>

<body>
  <?php
  $this->load->view('navbar');
  $this->load->view('marquee');
  $this->load->view('home/slider-home');
  $this->load->view('home/content');
  $this->load->view('footer');
  ?>

</body>

<!-- JQuery -->
<script type="text/javascript" src="<?= base_url('assets/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 JS -->
<script type="text/javascript" src="<?= base_url('assets/bootstrap4/js/bootstrap.min.js'); ?>"></script>


</html>