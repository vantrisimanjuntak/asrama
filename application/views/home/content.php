<hr>
<div class="container-fluid mb-5">
    <h4 class="font-weight-bold text-center mt-4" style="font-family: 'Raleway', sans-serif">Mengapa memilih Asrama ?</h4>
    <div class="row justify-content-md-center ml-5 mr-5 mt-4 mb-5">
        <div class="col-sm-6 col-md-6 col-lg-4">
            <center><i class="fa fa-graduation-cap fa-4x" style="color: #8996AA" aria-hidden="true"></i></center>
            <hr>
            <p style="font-family: 'Amethysta', serif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-4">
            <center><i class="fa fa-leaf fa-4x align-middle" style="color: #8996AA" aria-hidden="true"></i></center>
            <hr>
            <p style="font-family: 'Amethysta', serif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
            <center><i class="fa fa-eye fa-4x" style="color: #8996AA" aria-hidden="true"></i></i></center>
            <hr>
            <p style="font-family: 'Amethysta', serif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </div>
    </div>
</div>
<hr>
<div class="container-fluid mb-5">
    <div class="row">
        <div class="col-md-9">
            <h3 class="mb-5 text-center" style="font-family: 'Noto Serif', serif; ">Berita Terbaru</h3>
            <div class="row">
                <?php foreach ($berita as $row) : ?>
                    <div class="col-sm-6 col-md-6 col-lg-4 mb-3">
                        <div class="card">
                            <img src="<?= base_url('image/news/' . $row['gambar']); ?>" alt="" class="card-img-top rounded mx-auto d-block" style="height:240px;">
                            <div class="card-body">
                                <a href="<?= base_url('berita/show_detail/' . $row['link']) ?>" style="text-decoration: none; color:black">
                                    <h5 style="font-family: 'Martel', serif"><?= $row['judul_berita']; ?></h5>
                                </a>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <small><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;<?= date('d M Y', strtotime($row['tanggal_posting'])); ?></small>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <small><i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp;<?= date('H:i:s', strtotime($row['tanggal_posting'])); ?></small>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <small><i class="fa fa-user" aria-hidden="true"></i> <?= $row['posisi']; ?></small>
                                    </div>
                                </div>
                                <p><?= substr($row['isi_berita'], 0, 300); ?><a href="<?= base_url('berita/show_detail/' . $row['link']); ?>" style="text-decoration: none;">&nbsp;...baca selengkapnya</a></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-3" style="border-left: 1px solid #DDE6F4 ">
            <?php include 'side.php'; ?>
        </div>
    </div>
</div>