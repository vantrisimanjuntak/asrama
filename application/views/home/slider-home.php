<div id="demo" class="carousel slide" data-ride="carousel">
	<ul class="carousel-indicators">
		<li data-target="#demo" data-slide-to="0" class="active"></li>
		<li data-target="#demo" data-slide-to="1"></li>
		<li data-target="#demo" data-slide-to="2"></li>
		<li data-target="#demo" data-slide-to="3"></li>
	</ul>

	<div class="carousel-inner" role="listbox">
		<div class="carousel-item active" id="small" style="background-image: url('image/LogoBaru.png'); ">
			<div class="carousel-caption">
				<h2 class=" display-5" style="font-family: 'Caladea', serif; color: #E32636; "><i>Ad Astra Per Diciplinam</i></h2>
				<p class="lead text-dark" style="font-family: 'Alice', serif;">Meraih Cita-cita Setinggi Bintang</p>
			</div>
		</div>
		<div class="carousel-item " style="background-image: url('image/plank.jpg'); ">
			<div class="carousel-caption ">
				<h2 class=" display-5" style="font-family: 'Caladea', serif; color: white; "><i>Selamat Datang di Asrama</i></h2>
			</div>
		</div>
		<div class="carousel-item " style="background-image: url('image/asrama-top.jpg'); ">
			<div class="carousel-caption">
				<h2 class=" display-5" style="font-family: 'Caladea', serif; color: white; ">Go Green</h2>
				<p class="lead">Menghijaukan Kembali Lingkungan Sekitar</p>
			</div>
		</div>
		<div class="carousel-item " style="background-image: url('image/misa-jumat.jpg'); ">
			<div class="carousel-caption">
				<h2 class=" display-5" style="font-family: 'Caladea', serif; color: white; "><i>Misa Jumat</i></h2>
				<p class="lead">Beriman dan Terdidik</p>
			</div>
		</div>

	</div>

	<a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon"></span>
	</a>
	<a class="carousel-control-next" href="#demo" role="button" data-slide="next">
		<span class="carousel-control-next-icon"></span>
	</a>
</div>