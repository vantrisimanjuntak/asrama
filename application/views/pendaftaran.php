<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<link rel="shortcut icon" href="http://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
	<title>Pendaftaran Siswa-Siswi | Asrama St. Albertus Magnus Aekkanopan</title>
</head>
<body>
	<div class="row">
		<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/Logo.png" style="width: 100%; height: 40%; ">
	</div>
	<!-- Navbar -->
	<nav class="navbar navbar-inverse navbar-expand-sm bg-dark navbar-dark sticky-top" data-spy="affix" data-offset="197">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav" style="font-family: Verdana">
			<li class="nav-item">
				<a class="nav-link" href="home">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="personalia">Personalia</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="sejarah">Sejarah</a>
			</li>
			<li class="nav-item">
				<a class= "nav-link" href="galeri">Galeri</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="contact">Contact</a>
			</li>
		</ul>
		</div>
	</nav>

	<marquee style="font-family: Arial; color: steelblue; font-size: 22px">SELAMAT DATANG DI WEBSITE ASRAMA SANTO ALBERTUS MAGNUS AEKKANOPAN</marquee>
	<br>
	<div class="container">
		<h5>Pendaftaran Siswa</h5>
			<br>
			<form action="">
				<div class="form-group">
					<label>Nama </label>
					<input type="text" class="form-control col-md-4">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<textarea class="form-control col-md-5" rows="5"></textarea>
				</div>
			</form>
		
	</div>
</body>
</html>