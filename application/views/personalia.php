<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  	<link rel="shorcut icon" href="http://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png">
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
	<style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
      z-index: 9999 !important;
  }

  .affix + .container-fluid {
      padding-top: 70px;
  }
  </style>
	<title>Personalia | Asrama Santo Albertus Magnus Aekkanopan</title>
</head>
<body>
	<div class="row">
		<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/Logo.png" style="width: 100%; height: 40%">
	</div>
	<nav class="navbar navbar-inverse navbar-expand-sm bg-dark navbar-dark sticky-top" data-spy="affix" data-offset="197">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav" style="font-family: Verdana">
			<li class="nav-item">
				<a class="nav-link" href="index">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="personalia">Personalia</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="sejarah">Sejarah</a>
			</li>
			<li class="nav-item">
				<a class= nav-link href="galeri">Galeri</a>
			</li>
			<li class="dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Contact Us
				<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Lokasi</a></li>
					<li><a href="#">Kontak</a></li>
				</ul>
				</li>
			</ul>
		</div>
	</nav>
	<marquee style="font-family: Arial; color: steelblue; font-size: 22px">SELAMAT DATANG DI WEBSITE ASRAMA SANTO ALBERTUS MAGNUS AEKKANOPAN</marquee>
	<br>
	<!-- Main Page -->
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="container-fluid">
					<div class="container">
						<h2 style="font-family: Georgia; font-size: 23px; color: black">Personalia</h2><br>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Pemilik</th>
									<td>
										<a href="http://archdioceseofmedan.or.id" style="text-decoration: none; font-family: Times New Roman"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/logo_kam.jpg" style="height: 50px; width: 50px">Keuskupan Agung Medan</a>
									</td>
								</tr>
								<tr>
									<th>Stokeholders</th>
									<td>
										<a href="http://www.medan.kapusin.org" style="text-decoration: none; font-family: Times New Roman"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/kapusin.jpg" style="height: 50px; width: 50px"> Kapusin Propinsi Medan (Koordinator Komisi Asrama)</a>
									</td>
								</tr>
								<tr>
									<th>Penanggungjawab</th>
									<td>
										<a href="http://parokiaekkanopan.blogspot.co.id/p/profil_186.html" style="text-decoration: none;font-family: Times New Roman">Pastor Paroki St. Pius X Aekkanopan</a>
									</td>
								</tr>
								<tr>
									<th>Ketua</th>
									<td>Fery Sandra Berutu</td>
								</tr>
								<tr>
									<th>Wakil Ketua</th>
									<td>Pandenaker Simanjuntak</td>
								</tr>
							</thead>
						</table>
					</div>
					<br>
					<div class="container">
						<h2 style="font-style: Verdana; font-size: 18px; color: black">Uraian Tanggung Jawab Secara Umum</h2>
						<div class="accordion">
							<div class="card">
								<div class="card-header">
									<a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#penanggungjawab">Penanggung Jawab</a>
								</div>
								<div id="penanggungjawab" class="collapse hidden">
									<div class="card-body">
										<table style="width: 100%" class="table" border="1px solid black">
											<tr>
												<thead class="thead-light">
													<tr>
														<th style="text-align: center;">Profil</th>
														<th style="text-align: center;">Tugas</th>
													</tr>
												</thead>
												<tbody>
													<td>
														<div class="card" style="width: 400px">
															<img class="card-img-top" src="http://asramasantoalbertusmagnus.000webhostapp.com/image/stefanus.jpg">
															<div class="card-body">
																<h4 class="card-title">RP. Stefanus, OFMCap</h4>
															</div>
														</div>
													</td>
													<td>
														<ol type="a">
															<li style="text-align: justify;">Sebagai pastor paroki (wakil pemilik) dan sekaligus koordinator Komisi Asrama Kapusin (wakil badan pengutus),  bertanggung jawab atas segala hal yang terjadi di asrama. Pertanggungjawabannya terarah baik ke pihak orangtua anak, yayasan / sekolah, masyarakat maupun ke  lingkup Gerejani</li>
															<br>
															<li style="text-align: justify;">Sebagai wakil pemilik dan wakil badan pengutus  berhak untuk merestui dan menolak serta mengevaluasi segala kebijakan yang dibuat dan yang akan dibuat oleh koordinator dan para Pembina</li>
															<br>
															<li style="text-align: justify;">Sebagai wakil pemilik dan wakil badan pengutus  berhak untuk merestui dan menolak serta mengevaluasi segala kebijakan yang dibuat dan yang akan dibuat oleh koordinator dan para Pembina</li>
															<br>
															<li style="text-align: justify;">Bersama dengan koordinator dan para Pembina terutama dengan ekonom pelan-pelan merancang pembenahan-pembenahan bangunan yang laik pakai  dan fasilitas lainnya</li>
														</ol>
													</td>
												</tbody>
											</tr>
										</table>
									</div>
								</div>
								<div class="card-header">
									<a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#ketua">Ketua</a>
								</div>
								<div id="ketua" class="collapse hidde">
									<div class="card-body">
										<table style="width: 100%" class="table" border="1px solid black">
											<tr>
												<thead class="thead-light">
													<tr>
														<th style="text-align: center;">Profil</th>
														<th style="text-align: center;">Tugas</th>
													</tr>
												</thead>
												<tbody>
													<td>
														<div class="card" style="width: 400px">
															<img class="card-img-top" src="http://asramasantoalbertusmagnus.000webhostapp.com/image/berutu.jpg">
															<div class="card-body">
																<h4 class="card-title">Fery Sandra Berutu</h4>
															</div>
														</div>
													</td>
													<td>
														<ol type="a">
															<li style="text-align: justify;">Sebagai wakil dan perpanjangan tangan dari koordinator mengatur kerjasama yang baik dengan semua Pembina dan karyawan</li>
															<br>
															<li style="text-align: justify">Secara khusus bersama koordinator berkoodinasi untuk menata program harian, mingguan, bulanan, semesteran dan tahunan serta menjamin pelaksanaanya dan kemudian mempertanggungjawabkannya kepada penanggungjawab.</li>
															<br>
															<li style="text-align: justify;">Mengelola keuangan dengan transparan serta mempertanggungjawabkannya kepada koordinator dan selanjutnya koordinator mempertanggungjawabkannya kepada penanggungjawab atau ekonom paroki untuk kemudian dilaporkan ke Keuskupan Agung Medan</li>
															<br>
															<li style="text-align: justify">Bekerja   sama   dengan   Penanggungjawab dapur    mengatur     menu dan urusan perbelanjaan dapur</li>
															<br>
															<li style="text-align: justify">Bekerjasama dengan ketua dan koordinator merancang pembenahan-pembenahan fasilitas yang dibutuhkan terutama fasilitas yang sangat mendesak dibutuhkan segera</li>
															<br>
															<li style="text-align: justify">Membuat laporan keuangan sesuai dengan system pembukuan paroki atau KAM</li>
														</ol>
													</td>
												</tbody>
											</tr>
										</table>
									</div>
								</div>
								<div class="card-header">
									<a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#bendahara">Bendahara</a>
								</div>
								<div id="bendahara" class="collapse hidden">
									<div class="card-body">
										<table style="width: 100%">
											<tr>
												<th>
													<img src="" style="width: 100px">
												</th>
												<td>
													
												</td>
											</tr>
										</table>		
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<h2 style="font-style: Verdana; font-size: 18px; color: black">Pendampingan Anak Menurut Kelompok Minat /Bakat </h2>
					<p style="text-align: justify;">Untuk tahap awal ini pendampingan anak sesuai kelompok minat / bakatnya kita batasi diri dulu terhadap : Latihan Kepemimpinan; Latihan Seni dan Olahrga dan Bahasa Inggris dan Pemacuan anak berprestasi di sekolah.
    				Untuk tahap pertama dibuat penjaringan dan pengelompokannya kemudian diatur bagaimana mereka didampingi.   Jika kondisi keuangan dan ritme keseharian kita sudah maju selangkah lebih baik, kita akan mencoba menambah kelompok lain seperti bakat computer dengan on line internet dan minat dalam bidang perpustakaan.</p>
				</div>
			</div>
			<h2><a href="v_tampil">Siswa</a></h2>
			<div class="col-sm-3" style="height: 190px">
				<h5 style="font-family: Palatino Linotype; text-align: left">Info</h5>
				<hr>
				<div class="list-group-item list-group-item-action">
					<a href="https://drive.google.com/file/d/1dBmirzLKFqhGgNrZFXeknWDF1XpWjyud/view?usp=sharing" class="list-group-item-action">Buku Panduan</a>
				</div>
				<div class="list-group-item list-group-item-action">
				<a href="">Jadwal Asrama</a>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
			<div class="container-fluid" style="border: 1px solid black; background-color: black">
				<p style="color: white">Join with us :</p>
				<a href="https://www.facebook.com/Asrama-Santo-Albertus-Magnus-Aekkanopan-168400740445718/?modal=admin_todo_tour" style="text-decoration: none; color: white;"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/fb.png" style="height: 30px; width: 30px;"> Asrama Santo Albertus Magnus</a><br>
				<a href="https://www.instagram.com/asramasantoalbertus/?hl=id" style="text-decoration: none; color: white;"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/instagram.jpg" style="height: 30px; width: 30px; margin-top: 4px"> @asramasantoalbertusmagnus</a><br><br>
				<p style="color: white">&copy 2017-<?php echo date ("Y");?> Asrama Santo Albertus Magnus Aekkanopan. All Right Reserved</p>
			</div>
	</div>
</body>
</html>