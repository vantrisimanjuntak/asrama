<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<link rel="shortcut icon" href="http://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
	<title>Sejarah | Asrama St. Albertus Magnus Aekkanopan</title>
</head>
<body>
	<div class="row">
		<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/Logo.png" style="width: 100%; height: 40%">
	</div>
	<nav class="navbar navbar-inverse navbar-expand-sm bg-dark navbar-dark sticky-top" data-spy="affix" data-offset="197">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav" style="font-family: Verdana">
			<li class="nav-item">
				<a class="nav-link" href="home">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="personalia">Personalia</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="sejarah">Sejarah</a>
			</li>
			<li class="nav-item">
				<a class= nav-link href="galeri">Galeri</a>
			</li>
			<li class="dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Contact Us
				<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Lokasi</a></li>
					<li><a href="#">Kontak</a></li>
				</ul>
				</li>
			</ul>
		</div>
	</nav>
	<marquee style="font-family: Arial; color: steelblue; font-size: 22px">SELAMAT DATANG DI WEBSITE ASRAMA SANTO ALBERTUS MAGNUS AEKKANOPAN</marquee>
	<img src="https://asramasantoalbertusmagnus.000webhostapp.com/image/IMG20180209170033.jpg" style="width: 100%;">
	<br><br>
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="container-fluid">
					<div class="container">
						<img src="pengantar-icon.png" style="height: 120px; width: 280px;">
						<p style="text-align: justify;">Di paroki St. Pius X Aekkanopan, ada satu asrama, namanya Asrama Putra-Putri St. Albertus Magnus Aekkanopan. Salah satu tujuan pendirian asrama ini adalah untuk membantu umat katolik khususnya dan masyarakat umumnya yang terbelakang dalam mendukung proses pendidikan anak-anak mereka.</p>
						<br>
						<img src="motto-icon.png" style="height: 120px; width: 280px;">
						<p style="text-align: justify;">Motto Asrama St. Albertus Agung Aekkanopan ialah “<i>ad astram per disciplinam</i>” (Menggapai Cita Lewat Disiplin)</p>
						<br>
						<img src="visi-icon.png" style="height: 120px; width: 280px;">
						<p style="text-align: justify;">Asrama St. Albertus Magnus sebagai komunitas pendidikan bagi para pelajar yang hidup dalam kesadaran dan semangat kekeluargaan serta terus menerus berupaya meningkatkan keseimbangan hidup secara menyeluruh dalam keberimanan, kemandirian, kreativitas dengan disiplin yang tinggi untuk mempersiapkan masa depan cerah</p>
						<br>
						<img src="misi-icon.png" style="height: 120px; width: 280px;">
						<ol type="a">
							<li style="text-align: justify;"> Menghidupi semangat persaudaraan dan kekeluargaan</li>
							<li style="text-align: justify;"> Menyuburkan semangat belajar</li>
							<li style="text-align: justify;"> Menumbuhkan kesadaran akan pentingnya keseimbangan hidup duniawi dan rohani</li>
							<li style="text-align: justify;">  Membiasakan hidup disiplin demi masa depan yang cerah</li>
						</ol>
						<br>
						<h5 style="font-family: Times New Roman"><b>Sejarah Singkat</b></h5>
						<hr>
						<p style="text-align: justify;">Pada tahun 1971 sekolah katolik didirikan di Kampung Toba – Aekkanopan. Karena situasi tempat tidak mendukung bagi pengembangannya maka tahun 1974, tanah dibeli di Kampung Kristen – Aekkanopan. Tanah inilah yang sampai sekarang menjadi kompleks paroki, persekolahan Katolik dan Asrama St. Albertus Agung Aekkanopan.</p>
						<p style="text-align: justify;">Pada tahun 1975 di atas tanah yang dibeli di Kampung Kristen tersebut dibangun sekolah bersamaan dengan asrama. Dana pembangunan untuk sekolah dan asrama tersebut diperoleh dari P. Beatus Jenniskens OFMCap. dan P. Arie van Diemen OFMCap. Tujuan awal dari pembangunan asrama tersebut ialah untuk menampung para pelajar khususnya yang beragama Katolik yang berasal dari stasi-stasi yang sangat jauh dan susah ditempuh.</p>
						<p style="text-align: justify;">Dalam perjalanan waktu, asrama semakin dibutuhkan. Jumlah anak yang datang pun tidak seimbang lagi dengan kapasitas bangunan yang ada. Gedung yang dibangun pertama sekali tidak lagi sanggup menampung anak sekolah yang tinggal di asrama. Melihat situasi ini, maka pada tahun 1982, P. Gregory Sudiono, OFMCap. menambah bangunan asrama dan merehab beberapa ruangan lama. Walaupun demikian, dalam perkembangan selanjutnya, gedung masih tetap kurang memadai untuk menampung anak-anak yang semakin banyak berminat untuk tinggal dan didampingi di asrama.</p>
						<p style="text-align: justify;">Pada tahun 2000, P. Nelson Sitanggang OFMCap mulai bertugas sebagai pastor paroki di Paroki St. Pius X Aekkanopan ini. Pada tahun yang sama Beliau mengadakan rehab pada banguan asrama yang ada dan menambah satu gedung serba guna yang sangat sederhana yang difungsikan sebagai aula / ruang pertemuan tetapi karena keadaan menuntut maka ruang tersebut dipakai menjadi ruang belajar untuk putri dan ruang ibadat untuk setiap pagi dan malam karena sampai sekarang ruang doa belum ada</p>
						<p style="text-align: justify;">Berkat pertolongan Komisi Asrama Kapusin Medan yang dua kali menyelenggarakan loka karya terhadap para pendamping asrama di Keuskupan Agung Medan dan Keuskupan Sibolga, asrama ini sudah merumuskan visi dan misinya. Salah satu misi yang sudah terumus yang sekarang sedang diupayakan untuk menerapkannya ialah : Mengupayakan keseimbangan hidup duniawi dengan hidup rohani. Di tengah-tengah kesibukan belajar dan aktivitas lainya, anak-anak asrama disadarkan betapa pentingnya hidup rohani yang merupakan dasar berpijak untuk meraih cita-cita mereka</p>
						<p style="text-align: justify;">Akan tetapi, pada saat kita mengupayakan dan membiasakan anak-anak untuk cinta pada doa, dan memiliki minat untuk belajar, kami mengalami kesulitan karena ruang yang kondusif untuk doa dan ibadat rutin setiap hari, kami harus memakai ruang serbaguna sederhana tadi yang juga dipakai sebagai ruang belajar. Tahun 2006 P. Hiasintus Sinaga, OFMCap pindah dari Saribudolok ke Aekkanopan. Waktu itu Beliau memiliki tanggung jawab sebagai Koordinator Asrama-Asrama Kelolaan Kapusin Propinsi Medan (Asrama Putra St. Fransiskus Saribudolok, Asrama Putra St. Yohanes Pembaptis Pakkat dan Asrama Putra-Putri St. Albertus Magnus Aekkanopan). Sebagai koordinator asrama Kapusin Medan, Beliau cukup memberi perhatian kepada Asrama St. Albertus Magnus Aekkanopan. Melihat keadaan asrama yang jauh dari yang diharapkan sebagai pastor rekan di Aekkanopan, dia turut terjun dalam pembinaan dan ikut bekerja bersama pembina dan bersama dengan anak-anak dalam kehidupan harian asrama. Tekanan awal pastor ini ialah bahwa di asrama ini harus pelan-pelan ditegakkan disiplin, kebersihan dan minat belajar yang seimbang dengan pendampingan rohani</p>
						<p style="text-align: justify;">Motto asrama “ad astram per disciplinam” selalu menjadi refren pembinaan dan pembicaraan pastor ini dan rekan kerjanya (para pembina asrama). Pembiasaan untuk mengikuti aturan asrama terutama kesetiaan anak dan pembina mengikuti jadwal harian adalah sasaran utama untuk menegakkan kedisiplinan anak dan pembina. Dengan dengan hal ini, rapat kerja yang rutin baik di antara para pembina maupun dengan koordinator dan bahkan bersama dengan anak-anak atau ketua-ketua kelompok anak dilihat sebagai jalan untuk semakin mematrikan kedisiplinan dan kebersihan, minat belajar dan minat rohani. Sementara itu program jangka menengah dan jangka panjang mulai diuraikan dan strategi pencapaiannya juga dipaparkan. Salah satu program jangka menengah ialah pengagendaan kegiatan bina bakat anak dan pembinaan mental spiritual. Misalnya membuat “Temu Anak Asrama” dengan anak-anak asrama lain (dari lingkungan KAM) dalam bentuk malam seni dan kreasi. Sedangkan program jangka panjang ialah pembenahan-pembenahan fisik bangunan dan pengolahan kompleks serta pemantapan-pemantapan pendampingan dan pembinaan serta peningkatan pengelolaan asrama itu sendiri. Dengan demikian asrama menjadi tempat yang mendukung kepada pendidikan dan sekaligus ramah lingkungan. Fasilitas-fasilitas sedaya mampu dana yang diuapayakan berjalan secara bertahap seiring dengan pengelolaan yang semakin professional.</p>
					</div>
					<br><br>
					
  					<div id="fb-root"></div>
  					<script>
  						(function(d, s, id)
  						{
  							var js, fjs = d.getElementsByTagName(s)[0];
  							if (d.getElementById(id)) return;
  							js = d.createElement(s); js.id = id;
  							js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
  							fjs.parentNode.insertBefore(js, fjs);
  						}
  						(document, 'script', 'facebook-jssdk'));
  					</script>
  					
				</div>
			</div>
			<div class="col-sm-3" style="background-color:#eee1de; height: 300px">
				Info
				<hr>
				<div class="list-group-item list-group-item-action">
					<a href="https://drive.google.com/file/d/1dBmirzLKFqhGgNrZFXeknWDF1XpWjyud/view?usp=sharing" class="list-group-item-action">Buku Panduan</a>
				</div>
				<div class="list-group-item list-group-item-action">
					<a href="">Jadwal Asrama</a>
				</div>
			</div>
		</div>
	</div>
			</div>
		</div>
	<div class="footer-bottom">
			<div class="container-fluid" style="border: 1px solid black; background-color: black">
				<p style="color: white">Join with us :</p>
				<a href="https://www.facebook.com/Asrama-Santo-Albertus-Magnus-Aekkanopan-168400740445718/?modal=admin_todo_tour" style="text-decoration: none; color: white;"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/fb.png" style="height: 30px; width: 30px;"> Asrama Santo Albertus Magnus</a><br>
				<a href="https://www.instagram.com/asramasantoalbertus/?hl=id" style="text-decoration: none; color: white;"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/instagram.jpg" style="height: 30px; width: 30px; margin-top: 4px"> @asramasantoalbertusmagnus</a><br><br>
				<p style="color: white">&copy 2017-<?php echo date ("Y");?> Asrama Santo Albertus Magnus Aekkanopan. All Right Reserved</p>
			</div>
	</div>
</body>
</html>