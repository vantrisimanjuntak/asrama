<div class="container">
	<h4 style="font-family: Georgia; text-align: center;">Personalia</h4>
	<hr>
	<div class="card-deck border-0" style="font-family:'Times New Roman', Times, serif ;max-width:100%; max-height:100%">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="card text-center border-0">
					<div class="card-header">
						<h5>Pemilik</h5>
					</div>
					<div class="card-body">
						<img src="https://asramasantoalbertusmagnus.000webhostapp.com/image/logo_kam.jpg" style="max-width:200px;">
					</div>
					<div class="card-footer bg-transparent text-muted">
						<h5><a href="https://archdioceseofmedan.or.id/" style="text-decoration: none; color: black">Keuskupan Agung Medan</a></h5>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="card text-center border-0">
					<div class="card-header">
						<h5><i>Stakeholder</i></h5>
					</div>
					<div class="card-body">
						<img src="https://asramasantoalbertusmagnus.000webhostapp.com/image/kapusin.jpg" style="max-width:200px; max-height:200px;">
					</div>
					<div class="card-footer bg-transparent text-muted">
						<h5 style="margin-bottom: 20px;"><a href="https://archdioceseofmedan.or.id/" style="text-decoration: none; color: black">Kapusin Propinsi Medan</a></h5>
						<small>Koordinator Komisi Asrama</small>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="card text-center border-0">
					<div class="card-header">
						<h5>Penanggung Jawab</h5>
					</div>
					<div class="card-body">
						<img class="img-fluid" src="<?= base_url('image/logo-paroki.jpg') ?>" style=" height:200px;">
					</div>
					<div class="card-footer bg-transparent text-muted">
						<h5 style="margin-top: 20px;"><a href="https://archdioceseofmedan.or.id/" style="text-decoration: none; color: black">Pastor Paroki St. Pius X Aek Kanopan</a></h5>
					</div>
				</div>

			</div>
		</div>

	</div>


	<h4 style="font-family: Georgia; text-align: center; margin-top: 30px;">Uraian Tanggung Jawab Secara Umum</h4>
	<hr>
	<div id="accordion">
		<div class="card">
			<div class="card-header">
				<a class="card-link" data-toggle="collapse" href="#pastor_paroki">
					<center>Pastor Paroki</center>
				</a>
			</div>
			<div id="pastor_paroki" class="collapse" data-parent="#accordion">
				<div class="card-body">
					<table class="tables">
						<thead>
							<tr>
								<th style="text-align: center;">PROFIL</th>
								<th style="text-align: center;">URAIAN</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<img src="https://asramasantoalbertusmagnus.000webhostapp.com/image/stefanus.jpg" style="width: 280px; height: 220px;">
									<h5>RP. Stefanus, OFM.Cap</h5>
								</td>
								<td>
									<ol type="a">
										<li style="text-align: justify;">Sebagai Pastor Paroki (wakil pemilik) dan sekaligus Koordinator Komisi Asrama Kapusin (wakil badan pengutus), bertanggung jawab atas segala hal yang terjadi di asrama. Pertanggungjawabannya terarah baik ke pihak orangtua anak, yayasan/sekolah, masyarakat maupun lingkup gerejani.</li>
										<br>
										<li style="text-align: justify;">Sebagai wakil pemilik dan wakil badan pengutus berhak untuk merestui dan menolak serta mengevaluasi segala kebijakan yang dibuat dan yang akan dibuat oleh koordinator dan para Pembina</li>
										<br>
										<li style="text-align: justify;">Sebagai wakil pemilik dan wakil badan pengutus berhak untuk merestui dan menolak serta mengevaluasi segala kebijakan yang dibuat dan yang akan dibuat oleh koordinator dan para Pembina</li>
										<br>
										<li style="text-align: justify;">Sebagai wakil pemilik dan wakil badan pengutus berhak untuk merestui dan menolak serta mengevaluasi segala kebijakan yang dibuat dan yang akan dibuat oleh koordinator dan para Pembina</li>
										<br>
										<li style="text-align: justify;">Bersama dengan koordinator dan para Pembina terutama dengan ekonom pelan-pelan merancang pembenahan-pembenahan bangunan yang laik pakai dan fasilitas lainnya</li>
									</ol>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<a class="card-link" data-toggle="collapse" href="#ketua">
					<center>Ketua</center>
				</a>
			</div>
			<div id="ketua" class="collapse" data-parent="#accordion">
				<div class="card-body">
					<table class="tables">
						<thead>
							<tr>
								<th style="text-align: center;">PROFIL</th>
								<th style="text-align: center;">URAIAN</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<img src="https://asramasantoalbertusmagnus.000webhostapp.com/image/berutu.jpg" style="width: 280px; height: 220px;">
									<h5>Fery Sandra Brutu</h5>
								</td>
								<td>
									<ol type="a">
										<li style="text-align: justify;">Sebagai wakil dan perpanjangan tangan dari koordinator mengatur kerjasama yang baik dengan semua Pembina dan karyawan</li>
										<br>
										<li style="text-align: justify;">Secara khusus bersama koordinator berkoodinasi untuk menata program harian, mingguan, bulanan, semesteran dan tahunan serta menjamin pelaksanaanya dan kemudian mempertanggungjawabkannya kepada penanggungjawab.</li>
										<br>
										<li style="text-align: justify;">Mengelola keuangan dengan transparan serta mempertanggungjawabkannya kepada koordinator dan selanjutnya koordinator mempertanggungjawabkannya kepada penanggungjawab atau ekonom paroki untuk kemudian dilaporkan ke Keuskupan Agung Medan</li>
										<br>
										<li style="text-align: justify;">Bekerja sama dengan Penanggungjawab dapur mengatur menu dan urusan perbelanjaan dapur</li>
										<br>
										<li style="text-align: justify;">Bekerjasama dengan ketua dan koordinator merancang pembenahan-pembenahan fasilitas yang dibutuhkan terutama fasilitas yang sangat mendesak dibutuhkan segera</li>
										<br>
										<li style="text-align: justify;">Membuat laporan keuangan sesuai dengan system pembukuan paroki atau KAM</li>
									</ol>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>