<!DOCTYPE html>
<html>
<head>
	<title>Galeri | Asrama St. Albertus Magnus Aekkanopan</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://asramasantoalbertusmagnus.000webhostapp.com/owl.carousel.css">
	<link rel="stylesheet" href="https://asramasantoalbertusmagnus.000webhostapp.com/owl.theme.default.min.css">
	<link rel="shortcut icon" href="http://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="https://asramasantoalbertusmagnus.000webhostapp.com/jquery.min.js"></script>
  	<script  type="text/javascript" src="https://asramasantoalbertusmagnus.000webhostapp.com/owl.carousel.js"></script>
  	<style>
  		.row
  		{
  			margin: 15px;
  		}
  	</style>
</head>
<body>
	<div class="row">
		<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/Logo.png" style="width: 100%; height: 40%">
	</div>
	<nav class="navbar navbar-inverse navbar-expand-sm bg-dark navbar-dark sticky-top" data-spy="affix" data-offset="197">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav" style="font-family: Verdana">
			<li class="nav-item">
				<a class="nav-link" href="home">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="personalia">Personalia</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="sejarah">Sejarah</a>
			</li>
			<li class="nav-item">
				<a class= "nav-link active" href="galeri">Galeri</a>
			</li>
			<li class="dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Contact Us
				<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Lokasi</a></li>
					<li><a href="#">Kontak</a></li>
				</ul>
			</li>
		</ul>
		</div>
	</nav>
	<marquee style="font-family: Arial; color: steelblue; font-size: 22px">SELAMAT DATANG DI WEBSITE ASRAMA SANTO ALBERTUS MAGNUS AEKKANOPAN</marquee>
	<!-- Main Page -->
	<div class="container-fluid" style="height: auto;">
		<div class="row">
			<div class="col-sm-9">
				<h5 style="font-family: Times; font-size: 23px">Lingkungan Asrama</h5>
				<hr>
				<div class="owl-carousel owl-theme">
					<div class="item">
						<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/asrama_putra.jpg" width="100px" height="100px" class="img-fluid">
					</div>
					<div class="item">
						<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/IMG20180209170231.jpg" width="100px" height="100px" class="img-fluid">
					</div>
					<div class="item">
						<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/IMG20180209172134.jpg" width="100px" height="100px" class="img-fluid">
					</div>
					<div class="item">
						<img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/IMG20180209170108.jpg" width="100px" height="100px" class="img-fluid">
					</div>
				</div>
				<br>
				<div class="container">
					<p style="font-family: Arial">Asrama St. Albertus Magnus berada dalam satu kompleks dengan Gereja Katolik St. Pius X, Susteran KYM Aekkanopan, dan TK-SD-SMP SMA St. Yosep Aek Kanopan, yang dimana hal ini dapat menunjang proses pendidikan yang lebih baik lagi. Asrama ini memiliki satu gedung asrama pria, satu gedung asrama putri, kamar mandi, jemuran, dapur, ruang makan, ruang belajar, ruang rekreasi, ruang olahraga, gedung serbaguna, dan lapangan sepakbola.</p>
				</div>
				<br><br>
				<h5 style="font-family: Times New Roman; font-size: 23px">Kegiatan Sehari-Hari</h5>
				<hr>
				<div id="#jadwal">
					<div class="table-responsive-sm">
						<table class="table">
							<thead>
								<tr>
									<th>Jam</th>
									<th>Senin</th>
									<th>Selasa</th>
									<th>Rabu</th>
									<th>Kamis</th>
									<th>Jumat</th>
									<th>Sabtu</th>
									<th>Minggu</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>05.00</td>
									<td>Bangun Pagi</td>
									<td>Bangun Pagi</td>
									<td>Bangun Pagi</td>
									<td>Bangun Pagi</td>
									<td>Bangun Pagi</td>
									<td>Bangun Pagi</td>
									<td>Bangun Pagi</td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td>05.30-05.45</td>
									<td>Ibadat Pagi</td>
									<td>Kebersihan Pagi</td>
									<td>Ibadat Pagi</td>
									<td>Kebersihan Pagi</td>
									<td>Ibadat Pagi</td>
									<td>Ibadat Pagi</td>
									<td>Kebersihan Pagi</td>
								</tr>
							</tbody>
							<tbody>
								<tr>
											<td>05.45-06.00</td>
											<td>Belajar Pagi</td>
											<td>Misa di Gereja</td>
											<td>Belajar Pagi</td>
											<td>Misa di Gereja</td>
											<td>Belajar Pagi</td>
											<td>Belajar Pagi</td>
											<td>Kebersihan Pagi</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>06.00-06.25</td>
											<td>Kebersihan</td>
											<td style="text-align: center;">-</td>
											<td>Kebersihan</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center;">-</td>
											<td style="text-align: center;">-</td>
											<td style="text-align: center;">-</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>06.00-06.30</td>
											<td style="text-align: center;">-</td>
											<td>Misa Pagi</td>
											<td style="text-align: center;">-</td>
											<td>Misa Pagi</td>
											<td>Kebersihan</td>
											<td>Kebersihan</td>
											<td>Bangun Pagi</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>06.30-06.45</td>
											<td>Sarapan Pagi</td>
											<td>Sarapan Pagi</td>
											<td>Sarapan Pagi</td>
											<td>Sarapan Pagi</td>
											<td>Sarapan Pagi</td>
											<td>Sarapan Pagi</td>
											<td style="text-align: center">-</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>06.30-07.00</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td>Kebersihan</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>06.45-07.00</td>
											<td>Berangkat Sekolah</td>
											<td>Berangkat Sekolah</td>
											<td>Berangkat Sekolah</td>
											<td>Berangkat Sekolah</td>
											<td>Berangkat Sekolah</td>
											<td>Berangkat Sekolah</td>
											<td style="text-align: center">-</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>07.00-07.30</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td>Sarapan Pagi</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>07.30-07.45</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td style="text-align: center">-</td>
											<td>Berangkat Ke Gereja</td>
										</tr>
									</tbody>
									<tbody>
										<tr>
											<td>13.30-14.00</td>
											<td>Makan Siang</td>
											<td>Makan Siang</td>
											<td>Makan Siang</td>
											<td>Makan Siang</td>
											<td>Makan Siang</td>
											<td>Makan Siang</td>
										</tr>
									</tbody>
						</table>
					</div>
				</div>
				<br>
				<br>
				<!-- Galerry Main Page -->
				<h5 style="font-family: Times New Roman; font-size: 23px">Galeri</h5>
				<hr>
				<div class="card-deck">
					<div class="card">
						<img class="card-img-top" id="myImg" src="https://asramasantoalbertusmagnus.000webhostapp.com/image/ruang belajar.jpg">
						<div class="card-body">
							<h6 class="card-title">Ruang Belajar Asrama</h6>
						</div>
					</div>
					<!-- Modal -->
					<div id=""
					<div class="card">
						<img class="card-img-top" src="https://asramasantoalbertusmagnus.000webhostapp.com/image/ruang belajar2.jpg">
						<div class="card-body">
							<h6 class="card-title">Ruang Belajar</h6>
						</div>
					</div>
					<div class="card">
						<img class="card-img-top" src="https://asramasantoalbertusmagnus.000webhostapp.com/image/ruang belajar3.jpg">
						<div class="card-body">
							<h6 class="card-title">Ruang Belajar</h6>
						</div>
					</div>
				</div>
				<br>
				<div class="card-deck">
					<div class="card">
						<img class="card-img-top" src="https://asramasantoalbertusmagnus.000webhostapp.com/image/ruang olahraga.jpg">
						<div class="card-body">
							<h6 class="card-title">Ruang Olahraga</h6>
						</div>
					</div>
					<div class="card">
						<img class="card-img-top" src="https://asramasantoalbertusmagnus.000webhostapp.com/image/halaman depan.jpg">
						<div class="card-body">
							<h6 class="card-title">Halaman Depan Asrama</h6>
						</div>
					</div>
				</div>
				<br>
				<br>
				<!-- Video Page -->
				<h5 style="font-family: Times New Roman; font-size: 23px">Video</h5>
				<hr>
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="card" style="height: auto; width: 350px">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Mr8vM0zDy2o"></iframe>
								</div>
								<div class="card-body">
									<h6 class="card-title">
										<center>Sesi Latihan</center>
									</h6>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="card" style="height: auto; width: 350px">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe width="847" height="480" src="https://www.youtube.com/embed/wsXPArQmvkU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
									</iframe>
								</div>
								<div class="card-body">
									<h6 class="card-title">
										<center>Sesi Latihan</center>
									</h6>
								</div>
							</div>
						</div>
					</div>
						
						<div class="col-sm-4">
							<!-- Enter Video -->
						</div>
					</div>
				</div>
			<div class="col-sm-3">
				<h5 style="font-family: Palatino Linotype; text-align: left">Info</h5>
				<div class="list-group-item list-group-item-action">
					<a href="https://drive.google.com/file/d/1dBmirzLKFqhGgNrZFXeknWDF1XpWjyud/view?usp=sharing" class="list-group-item-action">Buku Panduan</a>
				</div>
				<div class="list-group-item list-group-item-action">
					<a href="#jadwal">Jadwal Asrama</a>
				</div>
			</div>
		</div>
	</div>	
	</div>
	<br>
	<div class="footer-bottom">
			<div class="container-fluid" style="border: 1px solid black; background-color: black">
				<p style="color: white">Join with us :</p>
				<a href="https://www.facebook.com/Asrama-Santo-Albertus-Magnus-Aekkanopan-168400740445718/?modal=admin_todo_tour" style="text-decoration: none; color: white;"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/fb.png" style="height: 30px; width: 30px;"> Asrama Santo Albertus Magnus</a><br>
				<a href="https://www.instagram.com/asramasantoalbertus/?hl=id" style="text-decoration: none; color: white;" style="text-decoration: none; color: white;"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/instagram.jpg" style="height: 30px; width: 30px; margin-top: 4px"> @asramasantoalbertusmagnus</a><br><br>
				<p style="color: white">&copy 2017-<?php echo date ("Y");?> Asrama Santo Albertus Magnus Aekkanopan. All Right Reserved</p>
			</div>
	</div>
	<script type="text/javascript" src="https:/asramasantoalbertusmagnus.000webhostapp.com/jquery.min.js"></script>
			<script type="text/javascript" src="https://asramasantoalbertusmagnus.000webhostapp.com/owl.carousel.js"></script>
			<script type="text/javascript">
				var owl = $('.owl-carousel');
				owl.owlCarousel({
    			items:4,
    			loop:true,
    			margin:10,
    			autoplay:true,
    			autoplayTimeout:1500,
    			autoplayHoverPause:true
				});
				$('.play').on('click',function(){
    				owl.trigger('play.owl.autoplay',[1000])
				})
				$('.stop').on('click',function(){
    			owl.trigger('stop.owl.autoplay')
				})
			</script>
</body>
</html>