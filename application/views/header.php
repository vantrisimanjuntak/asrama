<!doctype html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<link rel="shortcut icon" href="http://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png">
 	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/css_social-icon.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assets/css/main.js"></script>
  	<title>Galeri | Asrama St. Albertus Magnus Aekkanopan</title>
  </head>

  <body>
  	<header id="header">
		<!-- Nav -->
		<div id="nav">
			<div class="container">
				<!-- Logo -->
				<div class="nav-logo">
					<a href="" class="logo"><img src="http://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png" style="width: 100px; height: 100px;"></a>
				</div>
				<!-- Logo -->

				<!-- Nav -->
				<ul class="nav-menu nav navbar-nav">
					<li><a href="<?php echo base_url() ?>">Home</a></li>
					<li><a href="<?php echo base_url()?>personalia">Personalia</a></li>
					<li><a href="<?php echo base_url()?>sejarah">Sejarah</a></li>
					<li><a href="<?php echo base_url()?>galeri">Galeri</a></li>
					<li><a href="<?php echo base_url()?>call_us">Hubungi Kami</a></li>
				</ul>
				<!-- Nav -->

				<div class="nav-btns">
					<button class="aside-btn"><i class="fa fa-bars"></i></button>

				</div>
				<!-- Search Aside Toggle -->
			</div>
		</div>
		<!-- Main Nav -->

		<!-- Aside Nav -->
		<div id="nav-aside">
			<!-- nav -->
			<div class="section-row">
				<ul class="nav-aside-menu">
					<li><a href="#">Home</a></li>
          <li><a href="#">Personalia</a></li>
          <li><a href="#">Sejarah</a></li>
          <li><a href="#">Galeri</a></li>
          <li><a href="#">Hubungi Kami</a></li>
				</ul>
			</div>
		</div>
	</header>

	<marquee>
   		<p style="color: brown; font-family: Arial; font-size: 20px">SELAMAT DATANG DI WEBSITE ASRAMA ST. ALBERTUS MAGNUS AEKKANOPAN</p>
  	</marquee>
  </body>