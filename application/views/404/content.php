<div class="container-fluid" style=" margin-bottom: 30px;">
	<div class="row" style=" margin-top: 40px;">
		<div class="col-sm-8">
			<h1 class="text-center">404</h1>
			<p class="text-center">Halaman yang Anda tuju tidak tersedia</p>
		</div>

		<div class="col-sm-3" style="margin-bottom: 20px;">
			<?php $this->load->view('home/side'); ?>
		</div>
	</div>
</div>