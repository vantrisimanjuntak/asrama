<div class="container-fluid" style="background-color: #091f3e">
    <div class="row">
        <div class="col-md-2 col-sm-12 col-xs-12 " style="margin-bottom:10px;">
            <div class="" style="margin-top: 20px">
                <img src="<?php echo base_url('image/LogoBaru.png') ?>" class="img-fluid mx-auto d-block center-block" alt="" width="120px;" height="100px">
            </div>
        </div>
        <div class="col-md-3 col-sm-9 col-xs-4 " style="margin-top: 20px;">
            <h5 style="color: white">Asrama St. Albertus Magnus</h5>
            <p style="color: white">Jl. Serma Maulana No. 48 Aekkanopan, Sumatera Utara, Indonesia</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-9" style="margin-top: 20px;">
            <h5 style="color:white; font-family: 'Noto Sans', sans-serif">Hubungi Kami</h5>
            <i class="fas fa-phone" style="color:white;"> +62-853-5890-4344</i>
            <p style="font-size: 0.9rem;">
                <a href="mailto:psb@asramasantoalbertusmagnus.epizy.com" style="text-decoration:none;color:white">
                    <i class="far fa-envelope" style="color:white">&nbsp;psb@asramasantoalbertusmagnus.epizy.com</i>
                </a>
            </p>
        </div>
        <div class=" col-md-3" style="margin-top: 20px; margin-bottom:10px;">
            <h5 style="color: white">Ikuti Kami</h5>
            <a href="https://www.facebook.com/Asrama-Santo-Albertus-Magnus-Aekkanopan-168400740445718/">
                <i class="fab fa-facebook-f" style="color:white"> Asrama St. Albertus Magnus</i>
            </a>
            <a href="https://www.instagram.com/asramasantoalbertus/"><i class="fab fa-instagram" style="color:white"> Asrama St. Albertus Magnus</i></a>
        </div>
    </div>
    <div class="container-fluid" style="width:100% ;text-align:center; margin-top:14px;; padding-bottom:12px; color:white">
        <small>© <?= date('Y'); ?> Asrama St. Albertus Magnus Aek Kanopan. All Right Reserved</small>
    </div>
</div>