<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/bootstrap/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/v4-shims.css">
  <link href="https://fonts.googleapis.com/css?family=Marck+Script|Noto+Sans&display=swap" rel="stylesheet">
  <script type="text/javascript" src="<?= base_url('assets/js/jquery-3.3.1.slim.min.js') ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <title><?php echo $title ?></title>
  <style>
    .carousel-item {
      height: 100vhmin;
      min-height: 550px;
      background: no-repeat center center scroll;

      background-size: scroll;
      background-origin: content-box;
      -webkit-background-size: contain;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;


    }

    #small.carousel-item {


      height: 450px;
      background-origin: content-box;

      background: center no-repeat;
    }
  </style>
</head>

<body>



</body>

</html>