<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak PDF</title>
</head>

<body>
    <h1>
        <center>Cetak PDF</center>
    </h1>
    <table border="1">
        <tr>
            <td>No</td>
            <td>Nama</td>
            <td>Tanggal Lahir</td>
            <td>Alamat</td>
        </tr>
        <?php $no = 1;
        foreach ($record->result() as $row) : ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $row->nama ?></td>
                <td><?= $row->tanggal_lahir ?></td>
                <td><?= $row->alamat ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>

</html>