<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <div class="container">
    <div class="carousel-inner">
      <div class="carousel-item active ">
        <center><img src="<?php echo base_url('image/asrama-top.jpg') ?>" class="img-fluid" alt="" style="width:800px; margin:auto"></center>
        <div class="d-none d-sm-block carousel-caption" style="background-color: rgba(80, 100, 100, 0.5); float:left">
          <h5 style="font-size:2vw; font-family:Verdana, Geneva, Tahoma, sans-serif"><i>Go Green </i>Asrama</h5>
          <p class="d-flex justify-content-between" style="font-size:1.5vw">Penanaman pohon, kebersihan, penghijauan selalu diutamakan dalam setiap aspek kegiatan di asrama</p>
        </div>
      </div>
      <div class=" carousel-item">
        <center><img src="<?php echo base_url('image/ruang_belajar3.jpg') ?>" style="width:800px;" class="img-fluid" alt=""></center>
        <div class="d-none d-sm-block carousel-caption" style="background-color: rgba(80, 100, 100, 0.5); float:left">
          <h5 style="font-size:2vw; font-family:Verdana, Geneva, Tahoma, sans-serif"><i>Education other than school </i></h5>
          <p class="d-flex justify-content-between" style="font-size:1.5vw"></p>
        </div>
      </div>
      <div class="carousel-item ">
        <center><img src="<?php echo base_url('image/latihan.jpg') ?>" class="img-fluid" alt="" style="width:800px; margin:auto"></center>
        <div class="d-none d-sm-block carousel-caption" style="background-color: rgba(80, 100, 100, 0.5); float:left">
          <h5 style="font-size:2vw; font-family:Verdana, Geneva, Tahoma, sans-serif">Latihan Musik</h5>
        </div>
      </div>
    </div>
  </div>

  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<br>
<div class="container">
  <p>
    &nbsp;Asrama St. Albertus Magnus berada dalam satu kompleks dengan Gereja Katolik St. Pius X, Susteran KYM Aekkanopan, dan TK-SD-SMP SMA St. Yosep Aek Kanopan, yang dimana hal ini dapat menunjang proses pendidikan yang lebih baik lagi. Asrama ini memiliki satu gedung asrama pria, satu gedung asrama putri, kamar mandi, jemuran, dapur, ruang makan, ruang belajar, ruang rekreasi, ruang olahraga, gedung serbaguna, dan lapangan sepakbola.
  </p>
  <br>
  <h4 class="text-center">Lingkungan Asrama</h4>
  <hr>
  <section id="demos">
    <div class="row">
      <div class="col-md-12 columns">
        <div class="owl-carousel owl-theme">
          <div class="item">
            <img src="<?php echo base_url('image/asrama-front.jpg') ?>" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?php echo base_url('image/asrama-putra.jpg') ?>" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?php echo base_url('image/asrama-top.jpg') ?>" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?php echo base_url('image/IMG20180209170033.jpg') ?>" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?php echo base_url('image/ruang_belajar3.jpg') ?>" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?php echo base_url('image/ruang_olahraga.jpg') ?>" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?php echo base_url('image/futsal.jpg') ?>" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </section>
  <hr>
  <h4 class="text-center">Kerajinan Tangan</h4>
  <hr>
  <section id="demos">
    <div class="row">
      <div class="col-md-12 columns">
        <div class="owl-carousel owl-theme">
          <div class="item">
            <a data-flickr-embed="true" data-footer="true" href="https://www.flickr.com/gp/154995403@N08/4FfNDe" title="Replika Buaya"><img src="https://live.staticflickr.com/1740/41838499025_787b188e4e_t.jpg" width="100" height="56" alt="Replika Buaya"></a>
            <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
          </div>
          <div class="item">
            <a data-flickr-embed="true" data-footer="true" href="https://www.flickr.com/gp/154995403@N08/4FfNDe" title="Replika Buaya"><img src="https://live.staticflickr.com/1740/41838499025_787b188e4e_t.jpg" width="100" height="56" alt="Replika Buaya"></a>
            <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
          </div>
          <div class="item">
            <a data-flickr-embed="true" data-footer="true" href="https://www.flickr.com/gp/154995403@N08/4m6f8V" title="Replika Mulut Buaya"><img src="https://live.staticflickr.com/1743/27869120257_5bd500cb0f_t.jpg" width="56" height="100" alt="Replika Mulut Buaya"></a>
            <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
          </div>
          <div class="item">
            <a data-flickr-embed="true" data-footer="true" href="https://www.flickr.com/gp/154995403@N08/4Y0086" title="Replika Patung Sigale-gale"><img src="https://live.staticflickr.com/1739/42020878594_58300d6143_t.jpg" width="56" height="100" alt="Replika Patung Sigale-gale"></a>
            <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
          </div>
          <div class="item">
            <a data-flickr-embed="true" data-footer="true" href="https://www.flickr.com/gp/154995403@N08/M43x3H" title="Replika Patung Tiga Diri"><img src="https://live.staticflickr.com/1730/40927741690_94e03aa3ed_t.jpg" width="56" height="100" alt="Replika Patung Tiga Diri"></a>
            <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
          </div>
          <div class="item">
            <a data-flickr-embed="true" data-footer="true" href="https://www.flickr.com/gp/154995403@N08/zL19y9" title="Replika Kurcaci"><img src="https://live.staticflickr.com/1721/41839046735_2e10a8bdf9_t.jpg" width="56" height="100" alt="Replika Kurcaci"></a>
            <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
          </div>
          <div class="item">
            <a data-flickr-embed="true" data-footer="true" href="https://www.flickr.com/gp/154995403@N08/556AhS" title="Replika Kurcaci"><img src="https://live.staticflickr.com/1743/41838997395_83a7c8b13f_t.jpg" width="56" height="100" alt="Replika Kurcaci"></a>
            <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
          </div>
        </div>
      </div>
    </div>
  </section>
  <hr>
  <h4 class="text-center">Video Asrama</h4>
  <hr>
  <!-- Grid row -->
  <div class="row">
    <!-- Grid column -->

    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-4 col-md-6 mb-4">

      <!--Modal: Name-->
      <div class="modal fade" id="modal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <!--Content-->
          <div class="modal-content">

            <!--Body-->
            <div class="modal-body mb-0 p-0">

              <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Mr8vM0zDy2o" allowfullscreen></iframe>
              </div>

            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
              <span class="mr-4">Spread the word!</span>
              <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
              <!--Twitter-->
              <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
              <!--Google +-->
              <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
              <!--Linkedin-->
              <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

              <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

            </div>

          </div>
          <!--/.Content-->

        </div>
      </div>
      <!--Modal: Name-->

      <a><img class="img-fluid z-depth-1" src="<?php echo base_url('image/thumb_vid_1.jpg') ?>" alt="video" data-toggle="modal" data-target="#modal6"></a>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-4 col-md-6 mb-4">

      <!--Modal: Name-->
      <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">

          <!--Content-->
          <div class="modal-content">

            <!--Body-->
            <div class="modal-body mb-0 p-0">

              <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/wsXPArQmvkU" allowfullscreen></iframe>
              </div>

            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
              <span class="mr-4">Spread the word!</span>
              <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
              <!--Twitter-->
              <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
              <!--Google +-->
              <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
              <!--Linkedin-->
              <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

              <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

            </div>

          </div>
          <!--/.Content-->

        </div>
      </div>
      <!--Modal: Name-->

      <a><img class="img-fluid z-depth-1" src="<?php echo base_url('image/thumb_vid_2.jpg') ?>" alt="video" data-toggle="modal" data-target="#modal4"></a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->
  <hr>
</div>