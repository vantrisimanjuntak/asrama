<div class="container-fluid">
	<div class="row">
		<?php foreach ($berita as $row) {
		?>
			<div class="col-sm-9 col-md-8 col-lg-9 border-right mb-3">
				<div class="container-fluid">
					<h2 style="font-family: 'Martel', serif"><?= $row['judul_berita']; ?></h5>
						<div class="row">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
								<?php date_default_timezone_set('Asia/Jakarta'); ?>
								<small><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;<?= date('d-M-Y', strtotime($row['tanggal_posting'])); ?></small>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
								<small><i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp;<?= date('H:i:s', strtotime($row['tanggal_posting'])); ?></small>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
								<p><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?= $row['posisi']; ?></i></p>
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
								<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fasramasantoalbertusmagnus.000webhostapp.com&layout=button_count&size=small&width=81&height=20&appId" width="81" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
							</div>
						</div>
						<br>
						<img src="<?php echo base_url('image/news/' . $row['gambar']) ?>" alt="..." class="img-fluid mx-auto d-block " style="max-height:530px; margin-bottom: 30px;">
						<p><?php echo $row['isi_berita'] ?></p>
				</div>
			</div>

		<?php } ?>
		<div class="col-md-4 col-lg-3 col-xl-3" style="margin-bottom: 30px;">
			<?php include 'side.php' ?>
		</div>
	</div>
</div>