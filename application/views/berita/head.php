 <header>
    <!-- Nav -->
    <div id="nav">
      <!-- Main Nav -->
      <div id="nav-fixed">
        <div class="container">
          <!-- Logo -->
          <div class="nav-logo">
            <a href="<?php echo base_url()?>" class="logo"><img src="https://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png" style="width: 100px; height: 100px"></a>
          </div>
          <!-- Logo -->

          <!-- Nav -->
          <ul class="nav-menu nav navbar-nav">
            <li><a href="<?php echo base_url()?>">Home</a></li>
            <li><a href="<?php echo base_url()?>personalia">Personalia</a></li>
            <li><a href="<?php echo base_url()?>sejarah">Sejarah</a></li>
            <li><a href="<?php echo base_url()?>galeri">Galeri</a></li>
            <li><a href="<?php echo base_url()?>hubungi_kami">Hubungi Kami</a></li>
          </ul>
          <!-- Nav -->

          <!-- Search & side toggle -->
          <div class="nav-btns">
            <button class="aside-btn">
              <i class="fa fa-bars"></i>
            </button>
          </div>
          <!-- Search & side toggle -->

        </div>
      </div>
      <!-- Main Nav -->

      <!-- Side Nav -->

      <div id="nav-aside" style="width: 260px;">
        <!-- Nav -->
        <div class="section-row">
          <ul class="nav-aside-menu">
            <li><a href="<?php echo base_url()?>">Home</a></li>
            <li><a href="<?php echo base_url()?>personalia">Personalia</a></li>
            <li><a href="<?php echo base_url()?>sejarah">Sejarah</a></li>
            <li><a href="<?php echo base_url()?>galeri">Galeri</a></li>
            <li><a href="<?php echo base_url()?>hubungi_kami">Hubungi Kami</a></li>
          </ul>
        </div>
        <!-- Nav -->

        <button class="nav-aside-close"><i class="fa fa-times"></i></button>
      </div>

      <!-- Side Nav -->
    </div>
  </header>