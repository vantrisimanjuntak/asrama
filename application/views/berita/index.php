<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Shorcut Icon -->
	<link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
	<!-- Bootstrap 4 CSS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap4/css/bootstrap.min.css'); ?>">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Amethysta&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Alice&family=Caladea:ital@1&display=swap" rel="stylesheet">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>">
	<title><?= $title; ?></title>
</head>

<body>

	<?php
	$this->load->view('navbar');
	$this->load->view('marquee');
	$this->load->view('berita/show_detail');
	$this->load->view('footer');
	?>

</body>
<!-- jQuery -->
<script type="javascript" src="<?= base_url('assets/jquery-3.5.1.js') ?>"></script>
<!-- Bootstrap 4 JS -->
<script type="javascript" src="<?= base_url('assets/bootstrap4/js/bootstrap.min.js') ?>"></script>

</html>