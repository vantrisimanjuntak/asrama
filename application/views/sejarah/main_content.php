<div class="container text-center">
	<a href="<?= base_url('sejarah/#pengantar') ?>" style="text-decoration: none">
		<button type="button" id="btnPengantar" class="btn btn-success" style="width:100px;">Pengantar</button>
	</a>
	<a href="<?= base_url('sejarah/#motto'); ?>">
		<button type="button" class="btn btn-success" style="width:100px;">Motto</button>
	</a>
	<a href="<?= base_url('sejarah/#visi'); ?>">
		<button type="button" class="btn btn-success" style="width:100px;">Visi</button>
	</a>
	<a href="<?= base_url('sejarah/#misi'); ?>">
		<button type="button" class="btn btn-success" style="width:100px;">Misi</button>
	</a>
	<a href="<?= base_url('sejarah/#sejarah'); ?>">
		<button type="button" class="btn btn-success" style="width:140px;">Sejarah Singkat</button>
	</a>
</div>


<div style="margin-top:20px;">
	<div id="pengantar">
		<h3 style="font-family:Verdana, Geneva, Tahoma, sans-serif; font-size:2vm; text-align:center">Pengantar</h3>
		<hr style="width: 18em">
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">Di paroki St. Pius X Aekkanopan, ada satu asrama, namanya Asrama Putra-Putri St. Albertus Magnus Aekkanopan. Salah satu tujuan pendirian asrama ini adalah untuk membantu umat Katolik khususnya dan masyarakat umumnya yang terbelakang dalam mendukung proses pendidikan anak-anak mereka.</p>
	</div>
	<div id="motto">
		<h3 style="font-family:Verdana, Geneva, Tahoma, sans-serif; font-size: 2vm; text-align:center">Motto</h3>
		<hr style="width:18em">
		<p style="text-align: center; font-weight:bold ;font-size: 25px; font-family: 'Raleway', sans-serif"> “Ad Astram Per Disciplinam” (Menggapai Cita Lewat Disiplin) </p>
	</div>
	<div id="visi">
		<h3 style="font-family:Verdana, Geneva, Tahoma, sans-serif; font-size:2vm; text-align:center">Visi</h3>
		<hr style="width: 18em">
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Asrama St. Albertus Magnus sebagai komunitas pendidikan bagi para pelajar yang masih hidup dalam kesadaran dan semangat kekeluargaan serta terus menerus meningkatkan keseimbangan hidup secara menyeluruh dalam keberimanan, kemandirian, kreativitas dengan disiplin yang tinggi untuk mempersiapkan masa depan cerah.
		</p>
	</div>
	<div id="misi">
		<h3 style="font-family:Verdana, Geneva, Tahoma, sans-serif; font-size:2vm; text-align:center">Misi</h3>
		<hr style="width: 18em">
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			1. Menghidupi semangat persaudaraan dan kekeluargaan <br>
			2. Menyuburkan semangat belajar <br>
			3. Menumbuhkan kesadaran akan pentingnya keseimbangan hidup duniawi dan rohani <br>
			4. Membiasakan hidup disiplin demi masa depan yang cerah <br>
		</p>
	</div>
	<div id="sejarah">
		<h3 style="font-family:Verdana, Geneva, Tahoma, sans-serif; font-size:2vm; text-align:center">Sejarah Singkat</h3>
		<hr style="width: 18em">
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Pada tahun 1971 sekolah katolik didirikan di Kampung Toba – Aekkanopan. Karena situasi tempat tidak mendukung bagi pengembangannya maka tahun 1974, tanah dibeli di Kampung Kristen – Aekkanopan. Tanah inilah yang sampai sekarang menjadi kompleks paroki, persekolahan Katolik dan Asrama St. Albertus Agung Aekkanopan.
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Pada tahun 1975 di atas tanah yang dibeli di Kampung Kristen tersebut dibangun sekolah bersamaan dengan asrama. Dana pembangunan untuk sekolah dan asrama tersebut diperoleh dari P. Beatus Jenniskens OFMCap. dan P. Arie van Diemen OFMCap. Tujuan awal dari pembangunan asrama tersebut ialah untuk menampung para pelajar khususnya yang beragama Katolik yang berasal dari stasi-stasi yang sangat jauh dan susah ditempuh.
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Dalam perjalanan waktu, asrama semakin dibutuhkan. Jumlah anak yang datang pun tidak seimbang lagi dengan kapasitas bangunan yang ada. Gedung yang dibangun pertama sekali tidak lagi sanggup menampung anak sekolah yang tinggal di asrama. Melihat situasi ini, maka pada tahun 1982, P. Gregory Sudiono, OFMCap. menambah bangunan asrama dan merehab beberapa ruangan lama. Walaupun demikian, dalam perkembangan selanjutnya, gedung masih tetap kurang memadai untuk menampung anak-anak yang semakin banyak berminat untuk tinggal dan didampingi di asrama.
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Pada tahun 2000, P. Nelson Sitanggang OFMCap mulai bertugas sebagai pastor paroki di Paroki St. Pius X Aekkanopan ini. Pada tahun yang sama Beliau mengadakan rehab pada banguan asrama yang ada dan menambah satu gedung serba guna yang sangat sederhana yang difungsikan sebagai aula / ruang pertemuan tetapi karena keadaan menuntut maka ruang tersebut dipakai menjadi ruang belajar untuk putri dan ruang ibadat untuk setiap pagi dan malam karena sampai sekarang ruang doa belum ada
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Berkat pertolongan Komisi Asrama Kapusin Medan yang dua kali menyelenggarakan loka karya terhadap para pendamping asrama di Keuskupan Agung Medan dan Keuskupan Sibolga, asrama ini sudah merumuskan visi dan misinya. Salah satu misi yang sudah terumus yang sekarang sedang diupayakan untuk menerapkannya ialah : Mengupayakan keseimbangan hidup duniawi dengan hidup rohani. Di tengah-tengah kesibukan belajar dan aktivitas lainya, anak-anak asrama disadarkan betapa pentingnya hidup rohani yang merupakan dasar berpijak untuk meraih cita-cita mereka
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Akan tetapi, pada saat kita mengupayakan dan membiasakan anak-anak untuk cinta pada doa, dan memiliki minat untuk belajar, kami mengalami kesulitan karena ruang yang kondusif untuk doa dan ibadat rutin setiap hari, kami harus memakai ruang serbaguna sederhana tadi yang juga dipakai sebagai ruang belajar
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Tahun 2006 P. Hiasintus Sinaga, OFMCap pindah dari Saribudolok ke Aekkanopan. Waktu itu Beliau memiliki tanggung jawab sebagai Koordinator Asrama-Asrama Kelolaan Kapusin Propinsi Medan (Asrama Putra St. Fransiskus Saribudolok, Asrama Putra St. Yohanes Pembaptis Pakkat dan Asrama Putra-Putri St. Albertus Magnus Aekkanopan)
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Sebagai koordinator asrama Kapusin Medan, Beliau cukup memberi perhatian kepada Asrama St. Albertus Magnus Aekkanopan. Melihat keadaan asrama yang jauh dari yang diharapkan sebagai pastor rekan di Aekkanopan, dia turut terjun dalam pembinaan dan ikut bekerja bersama pembina dan bersama dengan anak-anak dalam kehidupan harian asrama. Tekanan awal pastor ini ialah bahwa di asrama ini harus pelan-pelan ditegakkan disiplin, kebersihan dan minat belajar yang seimbang dengan pendampingan rohani
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Motto asrama “ad astram per disciplinam” selalu menjadi refren pembinaan dan pembicaraan pastor ini dan rekan kerjanya (para pembina asrama). Pembiasaan untuk mengikuti aturan asrama terutama kesetiaan anak dan pembina mengikuti jadwal harian adalah sasaran utama untuk menegakkan kedisiplinan anak dan pembina. Dengan dengan hal ini, rapat kerja yang rutin baik di antara para pembina maupun dengan koordinator dan bahkan bersama dengan anak-anak atau ketua-ketua kelompok anak dilihat sebagai jalan untuk semakin mematrikan kedisiplinan dan kebersihan, minat belajar dan minat rohani
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Sementara itu program jangka menengah dan jangka panjang mulai diuraikan dan strategi pencapaiannya juga dipaparkan. Salah satu program jangka menengah ialah pengagendaan kegiatan bina bakat anak dan pembinaan mental spiritual. Misalnya membuat “Temu Anak Asrama” dengan anak-anak asrama lain (dari lingkungan KAM) dalam bentuk malam seni dan kreasi
		</p>
		<p style="text-align: justify; font-size: 1em; font-family: 'Amethysta', serif">
			Sedangkan program jangka panjang ialah pembenahan-pembenahan fisik bangunan dan pengolahan kompleks serta pemantapan-pemantapan pendampingan dan pembinaan serta peningkatan pengelolaan asrama itu sendiri. Dengan demikian asrama menjadi tempat yang mendukung kepada pendidikan dan sekaligus ramah lingkungan. Fasilitas-fasilitas sedaya mampu dana yang diuapayakan berjalan secara bertahap seiring dengan pengelolaan yang semakin professional
		</p>


	</div>
</div>