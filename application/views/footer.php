<div class="container-fluid" style="background-color:#0C3285">
    <div class="row justify-content-md-center p-3 text-white" style="font-family:'Amethysta', serif; ">
        <div class="col-md-12 col-lg-1 mb-3">
            <img src="<?= base_url('image/LogoBaru.png'); ?>" class="img-fluid mx-auto d-block" width="140px;" alt="">
        </div>
        <div class="col-md-4 mb-3">
            <h4>Asrama St. Albertus Magnus</h4>
            <h6"> Jl. Serma Maulana No. 48 Aekkanopan, Sumatera Utara, Indonesia</h6>
        </div>
        <div class="col-md-4 mb-3 align-center">
            <h5>Hubungi Kami</h5>
            <div class="row">
                <div class="col-sm-12">
                    <i class="fa fa-phone" style="color:white;"> +62-853-5890-4344</i>
                </div>
                <div class="col-sm-12">
                    <p style="font-size: 0.9rem;">
                        <a href="mailto:" style="text-decoration:none;color:white">
                            <i class="fa fa-envelope" style="color:white">&nbsp;asramasantoalbertusmagnus@yahoo.com</i>
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <h5>Ikuti Kami</h5>
            <div class="row">
                <div class="col-sm-12">
                    <a href="https://www.facebook.com/Asrama-Santo-Albertus-Magnus-Aekkanopan-168400740445718/">
                        <i class="fa fa-facebook-f" style="color:white"> Asrama St. Albertus Magnus</i>
                    </a>
                </div>
                <div class="col-sm-12">
                    <a href="https://www.instagram.com/asramasantoalbertus/"><i class="fa fa-instagram" style="color:white"> Asrama St. Albertus Magnus</i></a>
                </div>
            </div>
        </div>
        <h6 class="mt-4">©<?= date('Y'); ?>&nbsp;Asrama St. Albertus Magnus | Powered by Admin Asrama St. Albertus Magnus | All Right All Reserved </h6>
    </div>
</div>