<?php class Siswa extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Siswa_m');
        $this->load->library('session');
    }
    function index()
    {
        $data['title'] = "Siswa - Asrama St. Albertus Magnus";
        $this->load->view('siswa_v', $data);
    }
    function login()
    {
        $data['title'] = 'Login Siswa - Asrama St. Albertus Magnus';
        $this->load->view('portal_login', $data);
    }
    function auth()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $this->Siswa_m->auth($username, $password);
    }
    function logout()
    {
        $this->session->sess_destroy();
        redirect('admin/siswa');
    }
    function dashboard()
    {
        echo 'Hallo';
    }
}
