<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/siswa_template/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="<?= base_url('assets/siswa_template/vendor/datatables/dataTables.bootstrap4.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/siswa_template/css/sb-admin.css') ?>" rel="stylesheet">
    <title><?= $title ?></title>

</head>

<body class="bg-success">
    <div class=" mx-auto bg-white p-2 m-4 rounded shadow-lg p-3 mb-5 bg-white rounded col-xs-2 col-sm-7 col-md-6 col-lg-4"">
        <img src=" <?= base_url('image/LogoBaru.png') ?>" alt="Logo Asrama" class="mx-auto d-block" style="width:200px;">
        <h3 class="text-center m-4" style="font-family:Verdana">Login Siswa</h3>
        <hr>
        <div class="form-group">
            <form action="<?= base_url('siswa/auth') ?>" method="POST">
                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-at" aria-hidden="true"></i></span>
                    </div>
                    <input type="username" name="username" class="form-control" placeholder="Username">
                </div>
                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                    </div>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <button class=" btn btn-success col text-center" type="submit">LOGIN</button>
            </form>
            <?php
            $info = $this->session->flashdata('info');
            if (!empty($info)) {
                echo $info;
            }
            ?>
        </div>
        <hr>
    </div>
</body>

</html>