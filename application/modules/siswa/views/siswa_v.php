<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png') ?>">
    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/siswa_template/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="<?= base_url('assets/siswa_template/vendor/datatables/dataTables.bootstrap4.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/siswa_template/css/sb-admin.css') ?>" rel="stylesheet">
    <title><?= $title ?></title>
</head>

<body id="page-top">
    <?php
    $this->load->view('siswa_nav'); ?>
    <div id="wrapper">
        <?php
        $this->load->view('siswa_sidebar');
        $this->load->view('siswa_content'); ?>
    </div>


</body>

</html>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/siswa_template/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/siswa_template/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/siswa_template/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Page level plugin JavaScript-->
<script src="<?= base_url('assets/siswa_template/vendor/chart.js/Chart.min.js') ?>"></script>
<script src="<?= base_url('assets/siswa_template/vendor/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?= base_url('assets/siswa_template/vendor/datatables/dataTables.bootstrap4.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/siswa_template/js/sb-admin.min.js') ?>"></script>

<!-- Demo scripts for this page-->
<script src="<?= base_url('assets/siswa_template/js/demo/datatables-demo.js') ?>"></script>
<script src="<?= base_url('assets/siswa_template/js/demo/datatables-demo.js') ?>"></script>