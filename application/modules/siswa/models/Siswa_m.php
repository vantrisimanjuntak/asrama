<?php class Siswa_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function auth($username, $password)
    {
        $this->db->where('username', $username);
        $this->db->where('password', sha1($password));
        $query = $this->db->get('siswa');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $sess = array(
                    'username' => $row->username,
                    'nama' => $row->nama,
                    'pas_foto' => $row->pas_foto,
                    'nisn' => $row->nisn,
                );
                $this->session->set_userdata($sess);
            }
            redirect('siswa/dashboard');
        } else {
            $this->session->set_flashdata('info', 'Maaf, Username atau Password Salah');
            redirect('siswa/login');
        }
    }
}
