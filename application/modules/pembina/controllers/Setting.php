<?php class Setting extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    function index()
    {
        $data['title'] =  'Pengaturan - Portal Pembina ASAM';
        $this->load->view('setting/index', $data);
    }
}
