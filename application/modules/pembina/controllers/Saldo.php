<?php class Saldo extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pembina_model');
    }
    function index()
    {
        $data['title'] = "Saldo Siswa - Portal Pembina ASAM ";
        $data['profile'] = $this->Pembina_model->pembina_profile();
        $data['saldo'] = $this->Pembina_model->saldo();

        $this->load->view('saldo/index', $data);
    }
}
