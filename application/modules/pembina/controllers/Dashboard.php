<?php class Dashboard extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pembina_model');
        if (!$this->session->userdata('username')) {
            header("location:pembina");
        };
    }
    function index()
    {
        $data['title'] = 'Dashboard - Portal Pembina Asrama';
        $data['profile'] = $this->Pembina_model->pembina_profile();
        $data['total_siswa'] = $this->Pembina_model->show_siswa();
        $data['total_pembina'] = $this->Pembina_model->show_pembina();
        $data['total_berita'] = $this->Pembina_model->show_berita();
        $this->load->view('dashboard/index', $data);
        // $this->load->view('dashboard/content');
    }
}
