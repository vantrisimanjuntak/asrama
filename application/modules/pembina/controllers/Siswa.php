<?php class Siswa extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->Model('Pembina_model');
        if (!$this->session->userdata('username')) {
            redirect('pembina');
        }
    }
    function index()
    {
        $data['title'] = "Siswa - Portal Pembina ASAM";
        $data['profile'] = $this->Pembina_model->pembina_profile();
        $data['data_siswa'] = $this->Pembina_model->show_all_siswa();
        $this->load->view('siswa/index', $data);
    }
    function show_profile($nisn)
    {
        $uri_nisn = $this->uri->segment('4');
        $uri_pdf = $this->uri->segment('5');
        $check_nisn = $this->Model_model->show_profile($nisn);
        foreach ($check_nisn as $row) {
        }

        if ($uri_nisn === $row['nisn'] && $uri_pdf == FALSE) {


            $data['title'] = "Portal Pembina Asrama";
            $data['profile_siswa'] = $this->Model_model->show_profile($nisn);
            $this->load->view('siswa/show_profile/index', $data);
        } elseif ($uri_nisn == $row['nisn'] && $uri_pdf === 'pdf') {
            $this->db->where('nisn', $row['nisn']);
            $data['data_siswa'] = $this->db->get('siswa', $row['nisn'])->result_array();
            $this->load->view('siswa/show_profile/laporan_siswa', $data);
            $html = $this->output->get_output();
            $this->load->library('Dompdf_gen');
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            ob_end_clean();
            $this->dompdf->stream($nisn . ".pdf", array("Attachment" => 0));
            exit(0);
        } else {
            echo "ERROR";
        }
    }
    function pdf()
    {
        $data['total_siswa'] = $this->db->get('siswa')->result_array();
        $this->load->view('siswa/cetak_siswa', $data);
        $html = $this->output->get_output();
        $this->load->library('Dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $sekarang = date("d:F:Y:h:m:s");
        ob_end_clean();
        $this->dompdf->stream('Siswa Asrama St. Albertus Magnus.pdf', array("Attachment" => 0));
        exit(0);
    }
}
