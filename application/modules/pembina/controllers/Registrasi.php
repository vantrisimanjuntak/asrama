<?php class Registrasi extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Model_model');
    }
    function index()
    {
        $data['title'] = 'Registrasi Pembina | Portal ASAM';
        $data['gender'] = $this->Model_model->gender();
        $this->load->view('registrasi/index', $data);
        // $query = $this->db->get('status')->row();
        // $status = $query->status;

        // if ($status == 'ya') {
        //     $data['title'] = 'Registrasi Pembina | Portal ASAM';
        //     $this->load->view('registrasi/index', $data);
        // } else {
        //     echo "MAAF HALAMAN TIDAK BISA DIAKSES";
        // }
    }
    function submit()
    {
        $no_reg = $this->input->post('no_reg');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        // $foto = $this->input->post('userfile');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $username = md5($this->input->post('username'));
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        $this->Model_model->register($no_reg, $nama, $username, $password, $alamat, $tempat_lahir, $tanggal_lahir, $jenis_kelamin, $email);
    }
}
