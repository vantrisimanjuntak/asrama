<?php class Auth extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pembina_model');
        $this->load->library('session');
        $this->load->helper('url');
    }
    function index()
    {
        $id_login = $this->session->userdata('id_login');
        $query_session = $this->Pembina_model->get_session_login($id_login);
        if ($query_session) {
            redirect('pembina/dashboard');
        } else {
            $data['title'] = 'Login Pembina';
            $this->load->view('login/index.php', $data);
        }
    }
    function ceklogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $sha1_password = sha1($password);
        date_default_timezone_set('Asia/Jakarta');
        $t = date('Y-m-d H:i:s');
        $query_ceklogin = $this->Pembina_model->ceklogin($username, $sha1_password, $t);
        if ($query_ceklogin) {
            redirect('pembina/dashboard');
        } else if ($username == '' || $password == '') {
            $this->session->set_flashdata('emptyField', '<div class="alert alert-info text-center" role="alert">
                        Masukkan username dan password
                      </div>');
            redirect('pembina');
        } else {
            $this->session->set_flashdata('failedLogin', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Gagal Login!</strong> Maaf, username atau password Anda salah
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>');
            redirect('pembina');
        }
    }
    function forgot_password()
    {
        $id =  $token_reset_password = bin2hex(random_bytes(6));
        $email = $this->input->post('email');
        $username = $this->input->post('username');
        $token_reset_password = bin2hex(random_bytes(12));
        date_default_timezone_set('Asia/Jakarta');
        $time_start = date('Y-m-d H:i:s');
        $time_end = date('Y-m-d H:i:s', strtotime('+5 minutes', strtotime($time_start)));

        $query_reset_password = $this->Pembina_model->reset_password($id, $email, $username, $token_reset_password, $time_start, $time_end);


        $subject = 'RESET PASSWORD';
        $pesan = $token_reset_password;

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'simanjuntakvantri@gmail.com',
            'smtp_pass' => '24oktober1998',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            // 'smtp_crypto' => 'ssl'
        );

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from('simanjuntakvantri@gmail.com');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($pesan);

        if ($this->email->send()) {
            echo "EMAIL BERHASIL DIKIRIM";
        } else {
            show_error($this->email->print_debugger());
        }
    }
    function logout()
    {
        $id_login =  $this->session->userdata('id_login');
        print_r($id_login);
        date_default_timezone_set('Asia/Jakarta');
        $t = date('Y-m-d H:i:s');
        $query = $this->Pembina_model->logout($id_login, $t);
        $this->session->sess_destroy();
        redirect('pembina');
    }
}
