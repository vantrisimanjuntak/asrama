<?php class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('pembina');
        }
        $this->load->helper(array('form', 'url'));
        // $this->load->library('session', 'form_validation');
        $this->load->model('Pembina_model');
    }
    function index()
    {
        $data['title'] = $this->session->userdata('nama');
        $data['profile'] = $this->Pembina_model->pembina_profile();

        $this->load->view('profile/index', $data);
    }
    function update()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        // $this->form_validation->set_rules('userfile', 'Foto', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
            // redirect('pembina/profile');
        } else {
            $this->Model_model->update_profile();
            redirect('pembina/profile');
        }
    }
}
