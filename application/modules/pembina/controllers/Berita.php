<?php class Berita extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->Model('Model_model');
    }
    function index()
    {
        $data['title'] = "Berita | Portal Pembina ASAM";
        $data['profile'] = $this->Model_model->pembina_profile();
        $data['berita'] = $this->Model_model->getAllNews();


        $this->load->view('berita/index', $data);
    }
}
