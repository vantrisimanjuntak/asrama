<?php class Pembayaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pembina_model');
    }
    function index()
    {
        $data['title'] = "Pembayaran - Portal Pembina Asrama";
        $data['profile'] = $this->Pembina_model->pembina_profile();
        $data['datapembayaran'] = $this->Pembina_model->datapembayaran();
        $this->load->view('pembayaran/index', $data);
    }
    function statuspembayaran()
    {
        echo 'Oke';
    }
}
