<?php class All extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_model');
    }
    function index()
    {
        $data['title'] = "Pembina | Asrama St. Albertus Magnus";
        $this->load->view('pembina/index', $data);
    }
}
