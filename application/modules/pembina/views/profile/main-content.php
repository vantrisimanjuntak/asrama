<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- <h3 class="page-title">Panels</h3> -->

            <div class="alert alert-primary text-center" role="alert">
                <h3><?= $this->session->flashdata('form_empty'); ?></h3>
                <h5><?= validation_errors(); ?></h5>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- PANEL HEADLINE -->
                    <div class="panel panel-headline">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center ">PROFIL PEMBINA</h3>
                        </div>
                        <div class="panel-body">
                            <?php foreach ($profile as $p) {
                            ?>
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2" style="font-family:Arial">ID</label>
                                        <div class="col-sm-4">
                                            <?= $this->session->userdata('id'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2">
                                            <label class="font-weight-bold" style="font-family:Arial">Nama</label>
                                        </label>
                                        <div class="col-sm-6">
                                            <?= $p['nama']; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2" style="font-family:Arial">Alamat</label>
                                        <div class="col-sm-6">
                                            <?= $p['alamat']; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2" style="font-family:Arial">Tempat Lahir</label>
                                        <div class="col-sm-6">
                                            <?= $p['tempat_lahir']; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2" style="font-family:Arial">Tanggal Lahir</label>
                                        <div class="col-sm-6">
                                            <?= $p['tanggal_lahir']; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2" style="font-family:Arial">Jenis Kelamin</label>
                                        <div class="col-sm-6">
                                            <?= $p['jenis_kelamin']; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2" style="font-family:Arial">Email</label>
                                        <div class="col-sm-6">
                                            <?= $p['email']; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2" style="font-family:Arial">Username</label>
                                        <div class="col-sm-6">
                                            <?= $p['username']; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-md-9">Foto</label>
                                        <div class="col-sm-6">
                                            <img src="<?= base_url('image/profile/pembina_photo/'); ?><?= $p['pas_foto'] ?>" alt="" class="img-fluid" style="width:240px;">
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>


                        <!-- End Modal Profile -->
                        <button type="button" class="btn btn-warning btn-sm" style="margin-bottom: 10px; margin-left: 15px;" data-toggle="modal" data-target="#myModal">Perbarui Data</button>


                        <!-- Modal -->
                        <form action="<?= base_url(['pembina/profile/update']) ?>" enctype="multipart/form-data" method="POST">
                            <!-- Modal content-->
                            <div id="myModal" class="modal fade form-group row" role="dialog">
                                <div class="modal-dialog form-group row">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title font-weight-bold">Update Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class=" col-sm-4">
                                                    <label for="" class="">ID</label>
                                                </div>
                                                <div class="col-sm-7"><?= $p['id_pembina']; ?></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label for="">Nama</label>
                                                </div>
                                                <div class="col-sm-7">
                                                    <input type="text" name="nama" id="" class="form-control" value="<?= $p['nama_pembina']; ?>">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><label for="">Alamat</label></div>
                                                <div class="col-sm-7">
                                                    <textarea class="form-control" name="alamat" id="" cols="30" rows="10"><?= $p['alamat']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><label for="">Tempat Lahir</label> </div>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="tempat_lahir" id="" value="<?= $p['tempat_lahir']; ?>">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><label for="">Tanggal Lahir</label></div>
                                                <div class="col-sm-7">
                                                    <input type="date" class="form-control" name="tanggal_lahir" id="" value="<?= $p['tanggal_lahir']; ?>">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><label for="">Jenis Kelamin</label></div>
                                                <div class="col-sm-7">
                                                    <select name="jenis_kelamin" class="custom-select form-control" id="">
                                                        <option value="">--JENIS KELAMIN--</option>
                                                        <option value="Pria">PRIA</option>
                                                        <option value="Wanita">WANITA</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><label for="">Email</label></div>
                                                <div class="col-sm-7">
                                                    <input type="email" name="email" id="" class="form-control" value="<?= $p['email']; ?>">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"> <label for="">Password</label></div>
                                                <div class="col-sm-7">
                                                    <input type="password" class="form-control" name="password" id="password-field">
                                                    <span class="fa fa-fw fa-"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"> <label for="">Foto</label></div>
                                                <div class="col-sm-7">
                                                    <input type="file" name="userfile" id="">
                                                </div>
                                            </div>
                                        </div>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="submit" id="btnUpdate" class="btn btn-success">Perbarui</button>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->
            </div>



        </div>
    </div>
    <!-- END PANEL HEADLINE -->
</div>

</div>

</div>
</div>
<!-- END MAIN CONTENT -->
</div>

<script>
    $("#btnUpdate").click(function(e) {
        window.location.replace("http://stackoverflow.com");
    });
</script>