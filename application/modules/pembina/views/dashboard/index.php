<!doctype html>
<html lang="en">

<head>
	<title><?= $title ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?= base_url('assets/pembinacss/vendor/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/pembinacss/vendor/font-awesome/css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/pembinacss/vendor/linearicons/style.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/pembinacss/vendor/chartist/css/chartist-custom.css'); ?>">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?= base_url('assets/pembinacss/css/main.css'); ?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?= base_url('assets/pembinacss/css/demo.css'); ?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="https://asramasantoalbertusmagnus.000webhostapp.com/assets/pembinacss/image/LogoBaru.png">
	<link rel="icon" type="image/png" sizes="96x96" href="https://asramasantoalbertusmagnus.000webhostapp.com/assets/pembinacss/img/LogoBaru.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<?php
		$this->load->view('navbar');
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/main-content');
		$this->load->view('footer');
		?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?= base_url('assets/pembinacss/app.js') ?>"></script>
	<script src="<?= base_url('assets/pembinacss/vendor/jquery/jquery.min.js'); ?>"></script>
	<script src="<?= base_url('assets/pembinacss/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('assets/pembinacss/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script src="<?= base_url('assets/pembinacss/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js'); ?>"></script>
	<script src="<?= base_url('assets/pembinacss/vendor/chartist/js/chartist.min.js'); ?>"></script>
	<script src="<?= base_url('assets/pembinacss/scripts/klorofil-common.js'); ?>"></script>

</body>

</html>