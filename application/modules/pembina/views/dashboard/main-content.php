<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<!-- OVERVIEW -->
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">Weekly Overview</h3>
					<p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p>
				</div>
				<div class="panel-body">
					<div class="row">
						<a href="<?= base_url('pembina/siswa'); ?>" style="text-decoration:none">
							<div class="col-md-4" data-toggle="tooltip" title="Klik untuk melihat data siswa">
								<div class="metric">
									<span class="icon"><i class="fa fa-users"></i></span>
									<p>
										<span class="number"><?= $total_siswa; ?></span>
										<span class="title">Siswa/i</span>
									</p>
								</div>
							</div>
						</a>
						<a href="<?= base_url('pembina/pembina'); ?>" style="text-decoration:none">
							<div class="col-md-4" data-toggle="tooltip" title="Klik untuk melihat data pembina">
								<div class="metric">
									<span class="icon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
									<p>
										<span class="number"><?= $total_pembina; ?></span>
										<span class="title">Pembina</span>
									</p>
								</div>
							</div>
						</a>
						<a href="<?= base_url('pembina/berita'); ?>" style="text-decoration:none">
							<div class="col-md-4" data-toggle="tooltip" title="Klik disini untuk melihat berita">
								<div class="metric">
									<span class="icon"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
									<p>
										<span class="number"><?= $total_berita; ?></span>
										<span class="title">Berita</span>
									</p>
								</div>
							</div>
						</a>
						<!-- <div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="fa fa-bar-chart"></i></span>
								<p>
									<span class="number">35%</span>
									<span class="title">Conversions</span>
								</p>
							</div>
						</div> -->
					</div>
					<!-- <div class="row">
						<div class="col-md-9">
							<div id="headline-chart" class="ct-chart"></div>
						</div>
						<div class="col-md-3">
							<div class="weekly-summary text-right">
								<span class="number">2,315</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 12%</span>
								<span class="info-label">Total Sales</span>
							</div>
							<div class="weekly-summary text-right">
								<span class="number">$5,758</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 23%</span>
								<span class="info-label">Monthly Income</span>
							</div>
							<div class="weekly-summary text-right">
								<span class="number">$65,938</span> <span class="percentage"><i class="fa fa-caret-down text-danger"></i> 8%</span>
								<span class="info-label">Total Income</span>
							</div>
						</div>
					</div> -->
				</div>
			</div>

			<div class="row"></div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>