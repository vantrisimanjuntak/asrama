<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Font Awesome  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shorcut icon" href="https://asramasantoalbertusmagnus.000webhostapp.com/image/LogoBaru.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Shortcut Icon -->
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
    <!-- Font Google -->
    <link href="https://fonts.googleapis.com/css2?family=Taviraj&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title><?= $title; ?></title>
</head>

<body style="background-color: #EBEBE1">
    <div class="container bg-white p-2" style="margin-top:10px; border-radius: 4px;">
        <h3 style="font-family: 'Times New Roman'">Profil Siswa</h3>

        <?php foreach ($profile_siswa as $ps) {
        ?>
            <div class="container flex justify-content" style="margin-top: 30px;">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 170px;">NISN</th>
                                <td><?= $ps['nisn']; ?></td>
                            </tr>
                            <tr>
                                <th>NAMA</th>
                                <td><?= $ps['nama']; ?></td>
                            </tr>
                            <tr>
                                <th>JENIS KELAMIN</th>
                                <td><?= $ps['jenis_kelamin']; ?></td>
                            </tr>
                            <tr>
                                <th>TANGGAL LAHIR</th>
                                <td><?= date("d M Y", strtotime($ps['tanggal_lahir']));  ?></td>
                            </tr>
                            <tr>
                                <th>ALAMAT</th>
                                <td><?= $ps['alamat']; ?></td>
                            </tr>
                            <tr>
                                <th>Nama Ayah</th>
                                <td><?= $ps['nama_ayah']; ?></td>
                            </tr>
                            <tr>
                                <th>Nama Ibu</th>
                                <td><?= $ps['nama_ibu']; ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        <img src="<?= base_url('image/profile/siswa_photo/' . $ps['pas_foto']); ?>" class="img-fluid" style="max-width: 120px;" alt="">
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-warning float-right" style="color: black">
                        <a href="<?= base_url('pembina/siswa/show_profile/' . $ps['nisn'] . '/pdf'); ?>" style="text-decoration:none; color: white"><i class="fa fa-file-pdf-o" style="margin-right: 10px;" aria-hidden="true"></i>Download PDF</a></button>
                    <button class="btn btn-primary float-right" style="margin-right: 7px;"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Cetak</button>
                </div>
            </div>
        </div>
        <div class="container">
            Halo
        </div>
    </div>
</body>

</html>