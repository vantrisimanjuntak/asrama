<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="page-title">Siswa</div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Siswa Asrama</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped display" id="example">
                                <thead>
                                    <tr>
                                        <th>NISN</th>
                                        <th>Nama </th>
                                        <th>Tanggal Lahir</th>
                                        <th>Tempat Lahir</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat</th>
                                        <th>Username</th>
                                        <th>Password</th>
                                        <th>Nama Ayah</th>
                                        <th>Nama Ibu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data_siswa as $ds) {
                                    ?>
                                        <tr>
                                            <td>
                                                <a href="<?= base_url('pembina/siswa/show_profile/'); ?><?= $ds['nisn']; ?>"><?= $ds['nisn']; ?></td></a>
                                            <td><?= $ds['nama']; ?></td>
                                            <td><?= $ds['tanggal_lahir']; ?></td>
                                            <td><?= $ds['tempat_lahir']; ?></td>
                                            <td><?= $ds['jenis_kelamin']; ?></td>
                                            <td><?= $ds['alamat']; ?></td>
                                            <td><?= $ds['nama_ayah']; ?></td>
                                            <td><?= $ds['nama_ibu']; ?></td>
                                            <td><?= $ds['tempat_lahir']; ?></td>
                                            <td><?= $ds['jenis_kelamin']; ?></td>
                                        </tr>
                                    <?php }  ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="container-fluid" style="margin-top:15px;">
                            <div class="btn-block pull-right">
                                <button class="btn btn-warning pull-right" style="color: white">
                                    <a href="<?= base_url('pembina/siswa/pdf'); ?>" style="color:white"><i class="fa fa-file-pdf-o" style="margin-right: 10px; color: white" aria-hidden="true"></i>Download</a></button>
                                <button class="btn btn-primary pull-right" style="margin-right: 6px;"><i class="fa fa-print" style="margin-right: 10px;" aria-hidden="true"></i>Cetak</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>