<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        table {
            border-collapse: collapse;
            text-align: center;
        }

        table,
        td,
        th {
            border: 1px solid black;
        }

        .header {
            float: left;
            padding-bottom: 10px;

        }

        .header-text {
            text-align: center;
            float: left;
        }

        .header-text h2 {
            margin-left: 30px;
            margin-top: -70px;

        }

        .header-text p {
            padding-top: -60px;
            text-align: center;
        }

        img {
            margin-top: 0px;
            float: left;
            width: 100px;
        }
    </style>
    <title>Document</title>
</head><body>
    <div class="header">
        <img src="./image/LogoBaru.png" alt="">
        <div class="header-text">
            <h2 style="font-family: Times New Roman; text-align: center">Asrama St. Albertus Magnus </h2><br>
            <p>Jl. Serma Maulana No.49, Aek Kanopan, Labuhan Batu Utara, Sumatera Utara</p>
            <br>
            <p>Telp : 0623-4423 | Fax : 0623-4412</p>
            <br>
            <p>Email : asramaalbertusmagnus@yahoo.com</p>
            <hr>
        </div>
    </div>
    <h3 style="font-weight: bold; text-align:center">Daftar Nama Siswa/i Asrama</h3>
    <table style="border: 1px solid black; text-align:center">
        <tr>
            <td>NISN</td>
            <td>Nama</td>
            <td>Tanggal Lahir</td>
            <td>Tempat Lahir</td>
            <td>Jenis Kelamin</td>
            <td>Alamat</td>
        </tr>
        <?php foreach ($total_siswa as $row) : ?>
            <tr>
                <td><?= $row['nisn']; ?></td>
                <td><?= $row['nama']; ?></td>
                <td><?= $row['tanggal_lahir']; ?></td>
                <td><?= $row['tempat_lahir']; ?></td>
                <td><?= $row['jenis_kelamin']; ?></td>
                <td><?= $row['alamat']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body></html>