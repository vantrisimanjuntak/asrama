<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        table {
            border-collapse: collapse;
            text-align: center;
        }

        table,
        td,
        th {
            border: 1px solid black;
        }

        .grid-contianer {
            display: grid;
            grid-template-columns: auto auto auto auto;
            grid-gap: 10px;
            padding: 10px;
        }

        /* .grid-container>div {
            background-color: rgba(255, 255, 255, 0.8);
            text-align: center;
            padding: 20px 0;
            font-size: 30px;
        } */

        .image {
            border: 1px solid black;
        }
    </style>
    <title>Document</title>
</head>

<body>
    <div class="grid-container">
        <div class="item2"> <img src="image/LogoBaru.png" style=" width: 120px;" alt=""></div>
        <div class="item3">
            <h3 style="text-align: center">Siswa/i Asrama St. Albertus Magnus Aek Kanopan</h3>
        </div>



    </div>
    <table style="border: 1px solid black; text-align:center">
        <tr>
            <td>NISN</td>
            <td>NAMA</td>
            <td>JENIS KELAMIN</td>
            <td>TANGGAL LAHIR</td>
            <td>ALAMAT</td>
            <td>NAMA AYAH</td>
            <td>NAMA IBU</td>
        </tr>
        <?php foreach ($profil_siswa as $row) : ?>
            <tr>
                <td><?= $row['nisn']; ?></td>
                <td><?= $row['nama']; ?></td>
                <td><?= $row['jenis_kelamin']; ?></td>
                <td><?= $row['tanggal_lahir']; ?></td>
                <td><?= $row['nama_ayah']; ?></td>
                <td><?= $row['nama_ibu']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>

</html>