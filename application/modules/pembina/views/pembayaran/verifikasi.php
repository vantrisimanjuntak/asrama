<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <!-- BASIC TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="font-weight: bold">VERIFIKASI PEMBAYARAN</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php foreach ($datapembayar as $row) {
                            ?>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="">KODE BAYAR</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <?= $row['kode_bayar']; ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="">NISN</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <?= $row['nisn']; ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="">NAMA</label>
                                    </div>
                                    <?php foreach ($datapembayaran as $dp) {
                                    ?>
                                        <div class="col-sm-5">
                                            <?= $dp['nama']; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="">STATUS PEMBAYARAN</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <select name="" class="form-control" id="">
                                            <?php foreach ($datapembayar as $row) {
                                                $selected = ($row[''])
                                            ?>

                                            <?php } ?>
                                            <option value="0">Pending</option>
                                            <option value="1">Belum Bayar</option>
                                            <option value="2">Sudah Bayar</option>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <button type="button" class="btn btn-success">PERBARUI </button>
                        </div>
                    </div>
                    <!-- END BASIC TABLE -->
                </div>

            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>