<!doctype html>
<html lang="en">

<head>
    <title><?= $title ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/linearicons/style.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/chartist/css/chartist-custom.css'); ?>">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/pembinacss/css/main.css'); ?>">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?= base_url('assets/pembinacss/css/demo.css'); ?>">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="http://asam.epizy.com/image/LogoBaru.png">
    <link rel="icon" type="image/png" sizes="96x96" href="http://asam.epizy.com/image/LogoBaru.png">
    <!-- DataTables CSS-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/sweetalert/package/dist/sweetalert2.min.css'); ?>">
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <?php
        $this->load->view('navbar');
        $this->load->view('pembayaran/sidebar');
        $this->load->view('pembayaran/main-content');
        $this->load->view('footer');
        ?>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url('assets/style.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/chartist/js/chartist.min.js'); ?>"></script>
    <script src="<?= base_url('assets/scripts/klorofil-common.js'); ?>"></script>
    <!-- JS DataTables -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "scrollY": 200,
                "scrollX": true
            });
        });
    </script>
    <!-- SweetAlert2 JS -->
    <script src="<?= base_url('assets/sweetalert/package/dist/sweetalert2.all.min.js') ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

</body>

</html>