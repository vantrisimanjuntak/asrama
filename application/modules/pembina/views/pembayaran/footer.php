<footer>
    <div class="container-fluid">
        <p class="copyright">&copy; 2019 <a href="https://www.themeineed.com" target="_blank">Asrama St. Albertus Magnus</a>. All Rights Reserved.</p>
    </div>
</footer>