<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Siswa</h3>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <!-- BASIC TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="font-weight: bold">DATA PEMBAYARAN</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table id="example" class="display nowrap" style="width:100%">
                                <thead style="text-align: center">
                                    <tr>
                                        <th>Kode Pembayaran</th>
                                        <th>NISN</th>
                                        <th>Nama</th>
                                        <th>Waktu Pembayaran</th>
                                        <th>Penerima</th>
                                        <th>Pembayaran</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php foreach ($datapembayaran as $row) {
                                    ?>
                                        <tr>
                                            <td><a href="<?= base_url('pembayaran/kode_pembayaran/' . $row['kode_bayar']); ?>"><?= $row['kode_bayar']; ?></a>
                                            </td>
                                            <td><?= $row['nisn']; ?></td>
                                            <td><?= $row['nama']; ?></td>
                                            <td><?= date("d/m/Y", $row['waktu']); ?></td>
                                            <td><?= $row['nama_pembina']; ?></td>
                                            <td id="statuspembayaran"><?= $row['jumlah_pembayaran']; ?></td>
                                            <td>
                                                <?php if ($row['status_pembayaran'] == 'Pending') {
                                                    echo '  <div id="pending" style="background-color:yellow; border-radius:5px; text-align:center;font-weight:bold">
                                                    PENDING
                                                </div>';
                                                } else if ($row['status_pembayaran'] == 'Belum Bayar') {
                                                    echo '  <div id="belum_bayar" style="background-color:red; color:white; border-radius:5px; text-align:center;font-weight:bold">
                                                    BELUM BAYAR
                                                </div>';
                                                } else if ($row['status_pembayaran'] == 'Sudah Bayar') {
                                                    echo '
                                                      <div id="sudah_bayar" style="background-color:blue; color:white; border-radius:5px; text-align:center;font-weight:bold">
                                                        SUDAH BAYAR
                                                    </div>';
                                                } else {
                                                    echo "ERROR";
                                                } ?>
                                            </td>
                                            <td id="action_pembayaran">
                                                <button type="button" class="btn btn-warning batal">Batal</button>
                                                <!-- <a href=""><i class="fa fa-window-close fa-lg batal" style="color:red"></a></i> -->
                                                <button type="button" class="btn btn-primary terima">Terima</button>
                                                <!-- <a href="<?= base_url('pembayaran/accept/' . $row['kode_bayar']); ?>"><i class="fa fa-check-square fa-lg terima" style="color:green"></i></a> -->
                                            </td>
                                        </tr>
                                    <?php }  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END BASIC TABLE -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>