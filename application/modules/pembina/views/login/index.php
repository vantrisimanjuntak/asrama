<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap4/css/bootstrap.min.css'); ?>" type="text/css">
    <link rel="shorcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@600&display=swap" rel="stylesheet">
    <title><?= $title ?></title>
</head>

<body style="background-color: #26D4D2">
    <div class="container rounded col-lg-4 col-md-5 col-sm-8 col-xs-12 bg-white mx-auto p-4" style=" height:100%;margin-top:80px;margin-bottom:20px;">
        <img src="<?= base_url('image/LogoBaru.png') ?>" alt="" class="img-fluid mb-4 mx-auto d-block" style="width:160px;">
        <br><br>

        <?php
        echo $this->session->flashdata('emptyField');
        echo $this->session->flashdata('failedLogin'); ?>

        <form action="<?= base_url('pembina/auth/ceklogin'); ?>" method="POST" autocomplete="off">
            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label" style="font-family: 'Source Serif Pro', serif;">Username</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" name="username">
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label" style="font-family: 'Source Serif Pro', serif;">Password</label>
                <div class="col-sm-7">
                    <input type="password" name="password" class="form-control" id="">
                </div>
            </div>
            <div class="container-fluid">
                <div class="row mt-3 pt-3">
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <a href="" data-toggle="modal" data-target="#forgot_password" style="text-decoration: none; color:red">Lupa Password?</a>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <button type="submit" class="btn btn-primary" style="font-family: 'Source Serif Pro', serif;">Login</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="forgot_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Reset Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('pembina/auth/forgot_password') ?>" method="POST">
                        <div class="form-group row">
                            <div class="col-sm-12"><input class="form-control" type="email" name="email" placeholder="Email"></div>
                            <div class="col-sm-12"><input class="form-control" type="text" name="username" placeholder="Username"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</body>
<!-- jQuery JS -->
<script src="<?= base_url('assets/jquery-3.5.1.js'); ?>" type="text/javascript"></script>
<!-- Bootstrap 4 JS -->
<script src="<?= base_url('assets/bootstrap4/js/bootstrap.min.js') ?>"></script>

</html>