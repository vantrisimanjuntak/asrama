<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">Berita</h3>
                    <!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
                    <?php foreach ($berita as $row) : ?>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- PANEL DEFAULT -->
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><?= $row['judul_berita']; ?></h3>
                                        <div class="right">
                                            <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#<?= $row['id']; ?>"><i class="lnr lnr-chevron-up" style="color: blue"></i></button>
                                            <!-- <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button> -->
                                        </div>
                                    </div>
                                    <div class="panel-body" class="collapse" id="<?= $row['id'] ?>">
                                        <p><?= $row['isi_berita']; ?></p>
                                    </div>
                                    <div class="panel-footer">
                                        <small> <i> diposting oleh <?= $row['nama'] ?>&nbsp;pada tanggal <?= date('d-m-Y : H:i:s', strtotime($row['tanggal_posting'])); ?></i></small>
                                    </div>
                                </div>
                                <!-- END PANEL DEFAULT -->
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>

            <div class="row"></div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>