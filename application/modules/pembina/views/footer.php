<footer>
    <div class="container-fluid">
        <p class="copyright">&copy; <?= date('Y');?> Asrama St. Albertus Magnus Aek Kanopan | All Rights Reserved.</p>
    </div>
</footer>