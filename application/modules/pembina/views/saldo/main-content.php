<div class="main" style="color: black">
    <div class="main-content">
        <div class="container-fluid">
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 style="font-family: 'Source Sans Pro', sans-serif; color: black ">SALDO SISWA</h3>
                </div>
                <div class="panel-body">
                    <table id="example" class="table table-striped display" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <!-- <th>NISN</th> -->
                                <th>Nama</th>
                                <th>Jumlah Saldo</th>
                                <th>Waktu</th>
                                <th>Penerima</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($saldo as $saldo) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $saldo['id_saldo']; ?></td>
                                    <!-- <td><?= $saldo['nisn']; ?></td> -->
                                    <td><?= $saldo['nama']; ?></td>
                                    <td>Rp. <?= number_format($saldo['total_saldo'], 2, ',', '.'); ?></td>
                                    <td><?= date('d M Y H:i:s', strtotime($saldo['waktu'])); ?></td>
                                    <td><?= $saldo['nama_pembina']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>