<?php class Pembina_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function ceklogin($username, $sha1_password, $t)
    {
        $this->db->where('username', $username);
        $this->db->where('password', $sha1_password);

        $query = $this->db->get('account');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $sess = array(
                    'username' => $row['username'],
                    'nama' => $row['nama'],
                    'pas_foto' => $row['foto'],
                    'id' => $row['id'],
                    'logged_id' => TRUE,
                );
                $this->session->set_userdata($sess);

                $log_login = array(
                    'id_login' => bin2hex(random_bytes(7)),
                    'id_user' => $row['id'],
                    'login_time' => $t
                );
                $this->session->set_userdata($log_login);
                $this->db->insert('log_login', $log_login);
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function get_session_login($id_login)
    {
        $this->db->where('id_login', $id_login);
        $query = $this->db->get('log_login');
        return $query->result_array();
    }
    function logout($id_login, $t)
    {
        $this->db->set('logout_time', $t);
        $this->db->where('id_login', $id_login);
        $this->db->update('log_login');
    }


    // For Pembina
    function show_pembina()
    {
        $query = $this->db->get('pembina');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function pembina_profile()
    {
        $this->db->where('id', $this->session->userdata('id'));
        $query = $this->db->get('pembina');
        return $query->result_array();
    }
    function reset_password($id, $email, $username, $token_reset_password, $time_start, $time_end)
    {
        $data = array(
            'id' => $id,
            'email' => $email,
            'username' => $username,
            'token' => $token_reset_password,
            'start' => $time_start,
            'end' => $time_end,
            'username' => $username,
        );
        $this->db->insert('reset_password', $data);
    }



    // For Siswa
    function show_siswa()
    {
        $query = $this->db->get('siswa');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function show_all_siswa()
    {
        return $this->db->get('siswa')->result_array();
    }
    function saldo()
    {
        $this->db->select('*');
        $this->db->from('saldo_siswa a');
        $this->db->join('siswa b', 'b.no_reg = a.no_reg');
        $this->db->join('pembina c', 'c.id = a.penerima', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }


    // For Berita
    function show_berita()
    {
        $query = $this->db->get('berita');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }


    // For Pembayaran
    function datapembayaran()
    {
        $this->db->select('*');
        $this->db->from('pembayaran_siswa a');
        $this->db->join('siswa b', 'b.no_reg = a.no_reg');
        $this->db->join('pembina c', 'c.id = a.penerima', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }
}
