<?php class Home extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Registrasi_pembina_model');
    }
    function index()
    {
        $data['title'] = 'Registrasi Pembina | Portal ASAM';
        $data['gender'] = $this->Registrasi_pembina_model->gender();
        $data['posisi'] = $this->Registrasi_pembina_model->getPosisi();
        $this->load->view('registrasi_pembina/index', $data);
        // $query = $this->db->get('status')->row();
        // $status = $query->status;

        // if ($status == 'ya') {
        //     $data['title'] = 'Registrasi Pembina | Portal ASAM';
        //     $this->load->view('registrasi/index', $data);
        // } else {
        //     echo "MAAF HALAMAN TIDAK BISA DIAKSES";
        // }
    }
    function submit()
    {
        $no_reg = bin2hex(random_bytes(7));
        $nama = $this->input->post('nama');
        $posisi = $this->input->post('posisi');
        $email = $this->input->post('email');

        $param = $this->Registrasi_pembina_model->calon_registrasi($no_reg, $nama, $posisi, $email);

        if ($param) {
            echo "BERHASIL";
        } else {
            echo "GAGAL";
        }
    }
}
