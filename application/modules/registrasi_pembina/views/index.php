<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shorcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
    <!-- Bootstrap 4 CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title><?= $title; ?></title>
</head>

<body>
    <div class="container mt-2 mb-3" style="height:100%; border: 1px solid black; border-radius:5px">
        <!-- Header -->
        <div class="container-fluid mt-2">
            <div class="row">
                <div class="col-md-1 pt-3"> <img src="<?= base_url('image/LogoBaru.png'); ?>" class="mx-auto d-block" alt="" style="width:100px;">
                </div>
                <div class="col-md-11">
                    <div class="pl-4">
                        <h4 class="mt-2 text-left ">Asrama St. Albertus Magnus</h4>
                        <p style="margin-top: -9px;"><i>Ad Astra Per Diciplinam</i></p>
                        <p style="margin-top: -17px;">Jl. Serma Maulana No. 48 Aekkanopan, Sumatera Utara</p>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <!-- End Header -->
        <!-- Header -->
        <!-- Content -->

        <div class="container mt-4">
            <h5 class="text-center">Registrasi Pembina</h5>
            <hr style="width: 400px;">
            <small style="color:red;font-style:italic">*&nbsp;wajib diisi</small>
            <!-- Formulir -->

            <form action="<?= base_url('registrasi_pembina/home/submit'); ?>" method="POST" enctype="multipart/form-data">
                <div class="mt-3 pb-4">
                    <!-- <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">No. Reg<small>&nbsp;*</small></label>
                        <?php
                        $randNumb =  mt_rand(0, 100);
                        $no_reg = "P$randNumb";
                        ?>
                        <div class="col-sm-1">
                            <input type="text" name="no_reg" readonly class="form-control" value="<?= $no_reg; ?>" id="">
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Nama<small>&nbsp;*</small></label>
                        <div class="col-sm-8">
                            <input type="text" name="nama" class="form-control" id="" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Posisi<small>&nbsp;*</small> </label>
                        <div class="col-sm-3">
                            <select class="custom-select" name="posisi">
                                <?php foreach ($posisi as $ps) :  ?>
                                    <option value="<?= $ps['kode_posisi']; ?>"><?= $ps['posisi']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Alamat<small>&nbsp;*</small> </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="alamat" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div> -->
                    <!-- <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Tempat Lahir<small>&nbsp;*</small> </label>
                        <div class="col-sm-10">
                            <input type="text" name="tempat_lahir" class="form-control" id="">
                        </div>
                    </div> -->
                    <!-- <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Tanggal Lahir<small>&nbsp;*</small> </label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_lahir" class="form-control" id="">
                        </div>
                    </div> -->
                    <!-- <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Jenis Kelamin<small>&nbsp;*</small> </label>
                        <div class="col-sm-10">
                            <select class="custom-select" name="jenis_kelamin">
                                <?php foreach ($gender as $jk) :  ?>
                                    <option value="<?= $jk['kd_jenis_kelamin']; ?>"><?= $jk['jenis_kelamin']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Surat Lamaran<small>&nbsp;*</small></label>
                        <div class="col-sm-5">
                            <input type="file" accept="application/pdf,application/vnd.ms-excel" name="userfile" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-form-label col-sm-2">Email<small>&nbsp;*</small> </label>
                        <div class="col-sm-5">
                            <input type="email" name="email" class="form-control" id="">
                        </div>
                    </div>
                </div>
                <!-- End Formulir -->
                <!-- <div class="row container-fluid"> -->
                <div class="row">
                    <div class="col-lg-12 mb-2">
                        <button type="submit" class="btn btn-success float-right">Kirim </button>
                    </div>
                </div>

                <!-- </div> -->
            </form>
        </div>

        <!-- End Content -->
    </div>
</body>

<!-- Bootstrap 4 JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</html>