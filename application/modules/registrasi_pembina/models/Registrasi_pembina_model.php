<?php class Registrasi_pembina_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function calon_registrasi($no_reg, $nama, $posisi, $email)
    {
        date_default_timezone_set('Asia/Jakarta');
        $regTime = date('Y-m-d H:i:s');

        $data = array(
            'no_reg' => $no_reg,
            'nama' => $nama,
            'posisi' => $posisi,
            'file_lamaran' => $this->fileLamaran($no_reg),
            'email' => $email,
            'time' => $regTime,
        );

        if ($data) {
            $this->db->insert('calon_registrasi', $data);
            return TRUE;
        } else {
            return FALSE;
        }
    }
    private function fileLamaran($no_reg)
    {
        $config['upload_path'] = './files/lamaran/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '4096';
        // $config['max_width'] = '0';
        // $config['max_height'] = '0';
        $config['file_name'] = $no_reg;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            return $this->upload->data('file_name');
        }
    }
    function getPosisi()
    {
        return $this->db->get('posisi')->result_array();
    }
    // for pembina
    function register($id, $nama, $alamat, $tempat_lahir, $tanggal_lahir, $jenis_kelamin, $email)
    {
        date_default_timezone_set('Asia/Jakarta');
        $regTime = date('Y-m-d H:i:s');
        $data = array(
            'no_reg' => $id,
            'nama' => $nama,
            // 'username' => $username,
            // 'password' => $password,
            'alamat' => $alamat,
            // 'tempat_lahir' => $tempat_lahir,
            // 'tanggal_lahir' => $tanggal_lahir,
            // 'jenis_kelamin' => $jenis_kelamin,

            'email' => $email,
            // 'status' => 1,
            'registered_time' => $regTime,
        );
        // $this->db->insert('calon_pembina', $data);
        if ($data == true) {
            print_r($data);
        } else {
            echo "ERROR";
        }
        // redirect('pembina');
    }
    function ceklogin($username, $password, $t)
    {
        $this->db->where('username', $username);
        $this->db->where('password', sha1($password));

        $query = $this->db->get('pembina');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $sess = array(
                    'username' => $row->username,
                    'nama_pembina' => $row->nama_pembina,
                    'pas_foto' => $row->pas_foto,
                    'id_pembina' => $row->id_pembina,
                );
                $this->session->set_userdata($sess);

                $data = array(
                    'pembina_id' => $row->id_pembina,
                    'waktu_login' => $t,
                );
                $this->db->insert('log_login_pembina', $data);
            }
            redirect('pembina/dashboard');
        } else {
            $this->session->set_flashdata('info', 'Username dan Password Salah');
            redirect('pembina');
        }
    }
    function gender()
    {
        return $this->db->get('jenis_kelamin')->result_array();
    }
    // For Siswa
    function show_all_siswa()
    {
        return $this->db->get('siswa')->result_array();
    }
    function show_siswa()
    {
        $query = $this->db->get('siswa');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function show_profile($nisn)
    {
        $this->db->where('nisn', $nisn);
        $query = $this->db->get('siswa', $nisn);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            echo "DATA TIDAK ADA";
        }
    }
    function saldo()
    {
        $this->db->select('*');
        $this->db->from('saldo_siswa a');
        $this->db->join('siswa b', 'b.nisn = a.nisn');
        $this->db->join('pembina c', 'c.id_pembina = a.penerima', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }
    // Berita
    function getAllNews()
    {
        $this->db->from('login a');
        $this->db->join('berita b', 'a.id = b.author');
        $this->db->order_by('tanggal_posting', 'DESC');
        return $this->db->get()->result_array();
        return $this->db->get('berita')->result_array();
    }
    function show_berita()
    {
        $query = $this->db->get('berita');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function author()
    {
        $this->db->from('login a');
        $this->db->join('berita b', 'a.id = b.author');
        return $this->db->get('berita')->result_array();
        // return $this->db->get('berita')->result_array();
    }

    //function for pembina
    function pembina_profile()
    {
        $this->db->where('id_pembina', $this->session->userdata('id_pembina'));
        $query = $this->db->get('pembina');
        return $query->result_array();
    }
    function show_pembina()
    {
        $query = $this->db->get('pembina');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    // function update_profile()
    // {
    //     $nama = $this->input->post('nama');
    //     $alamat = $this->input->post('alamat');
    //     $tempat_lahir = $this->input->post('tempat_lahir');
    //     $jeniskelamin = $this->input->post('jenis_kelamin');
    //     $email = $this->input->post('email');
    //     $password = $this->input->post('password');
    //     $foto = $this->_uploadImage();

    //     $data = array(
    //         'nama_pembina' => $nama,
    //         'alamat' => $alamat,
    //         'tempat_lahir' => $tempat_lahir,
    //         'jenis_kelamin' => $jeniskelamin,
    //         'email' => $email,
    //         'password' => sha1($password),
    //         'pas_foto' => $foto,
    //     );
    //     $this->db->update('pembina', $data, array('id_pembina' => $this->session->userdata('id_pembina')));
    // }
    private function __uploadImage()
    {
        $config['upload_path'] = './image/profile/pembina_foto/';
        $config['allowed_types'] = 'png|jpeg|jpg|gif';
        $config['max_size'] = '2048';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = bin2hex(random_bytes(7));

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            return $this->upload->data('file_name');
        }
    }

    //function for pembayaran
    function datapembayaran()
    {
        $this->db->select('*');
        $this->db->from('pembayaran_siswa a');
        $this->db->join('siswa b', 'b.nisn = a.nisn');
        $this->db->join('pembina c', 'c.id_pembina = a.penerima', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }
    function show_datapembayar($kb)
    {
        if ($kb == TRUE) {
            $this->db->where('kode_bayar', $kb);
            return $this->db->get('pembayaran_siswa', $kb)->result_array();
        } else {
            echo "ERROR";
        }
    }
}
