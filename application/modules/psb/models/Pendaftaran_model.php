<?php class Pendaftaran_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function submitsiswa()
    {
        $nisn = $this->input->post('nisn');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $pas_foto = $this->_uploadImage();
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $tmpt_lahir = $this->input->post('tempat_lahir');
        $nama_ayah = $this->input->post('nama_ayah');
        $nama_ibu = $this->input->post('nama_ibu');
        // $email = $this->input->post('email');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $data = array(

            'nisn' => $nisn,
            'nama' => $nama,
            'pas_foto' => $pas_foto,
            'alamat' => $alamat,
            'jenis_kelamin' => $jenis_kelamin,
            'tanggal_lahir' => $tgl_lahir,
            'tempat_lahir' => $tmpt_lahir,
            'nama_ayah' => $nama_ayah,
            'nama_ibu' => $nama_ibu,
            // 'email' => $email,
            'username' => $username,
            'password' => sha1($password),
        );
        $this->db->insert('siswa', $data);
    }
    private function _uploadImage()
    {
        log_message("error", print_r($_FILES, true));
        $config['upload_path'] = './image/profile/siswa_photo/';
        $config['allowed_types'] = 'png|jpeg|jpg|gif';
        $config['max_size'] = '2048';
        $config['max_width'] = '0';
        $config['max_height'] = '0';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('upload_form', $error);
        } else {
            return $this->upload->data('file_name');
        }
    }
}
