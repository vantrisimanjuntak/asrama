<?php class Pendaftaran extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pendaftaran_model');
        $this->load->model('admin/Setting_model');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
    }
    function index()
    {
        date_default_timezone_set('Asia/jakarta');
        $t = time();
        $now = date("Y-m-d H:i:s", $t);
        $open =  $this->db->get('pendaftaran', 'DESC')->row();
        $end = $open->berakhir;

        if ($now < $end) {

            $data['title'] = 'Pendaftaran Siswa Baru | Asrama St. Albertus Magnus ';
            $this->load->view('index', $data);
        } else {
            echo 'Close';
        }
    }
    function submitsiswa()
    {
        $this->Pendaftaran_model->submitsiswa();
    }
}
