<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png'); ?>">
    <script src="<?= base_url('assets/js/app.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/jquery.min.js'); ?>"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <title><?= $title ?></title>
</head>

<body>
    <div class="container" style="margin-top:12px;">
        <div class="row" style="padding-top:18px; padding-bottom:18px; border-top-right-radius:6px; border-top-left-radius:6px ;background-color: blueviolet;">
            <div class="col-xs-2 col-md-2 col-sm-2">
                <img src="<?= base_url('image/LogoBaru.png') ?>" style="width: 100px;" alt="" class="mx-auto d-block img-fluid img-responsive">
            </div>
            <div class="col-xs-8 col-md-8 col-sm-8">
                <h5 style="color:white; font-weight:bold; text-align:center">PENDAFTARAN SISWA BARU</h5>
                <h5 style="color:white; font-weight:bold;text-align:center">ASRAMA ST. ALBERTUS MAGNUS AEK KANOPAN</h5>
            </div>
            <div class="col sm-2"></div>
        </div>
    </div>
    <?= form_open_multipart('psb/pendaftaran/submitsiswa'); ?>
    <div class="container" style="border-style:groove; height: 100%;">
        <p class="text-danger font-italic font-weight-bold" style="margin-top: 10px;">Harap mengisi form data dengar benar!</p><br>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">NISN <small>(Nomor Induk Siswa Nasional)</small></label>
            <div class="col-sm-7">
                <input type="text" name="nisn" id="" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-7">
                <input type="text" name="nama" id="" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-7">
                <textarea class="form-control" name="alamat" id="" cols="30" rows="10"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-5">
                <select class="form-control" name="jenis_kelamin" id="">
                    <option value="">--PILIH JENIS KELAMIN--</option>
                    <option value="1">Pria</option>
                    <option value="2">Wanita</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Tanggal Lahir</label>
            <div class="col-sm-5">
                <input type="date" name="tgl_lahir" id="" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Tempat Lahir</label>
            <div class="col-sm-7">
                <textarea name="tempat_lahir" id="" cols="30" rows="7" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="foto" class="col-sm-2 col-form-label">Pas Foto</label>
            <div class="col-sm-3">
                <input type="file" require name="userfile" id="" class="form-control-file">
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Nama Ayah</label>
            <div class="col-sm-7">
                <input type="text" name="nama_ayah" id="" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Nama Ibu</label>
            <div class="col-sm-7">
                <input type="text" name="nama_ibu" id="" class="form-control">
            </div>
        </div>
        <!-- <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-5">
                <input type="email" name="email" id="" class="form-control">
            </div>
        </div> -->
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label font-italic">Username</label>
            <div class="col-sm-5">
                <input type="text" name="username" id="" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label font-italic">Password</label>
            <div class="col-sm-5">
                <input type="password" name="password" id="" class="form-control">
            </div>
        </div>
        <div class="float-right">
            <button type="reset" id="reset" class="btn btn-dark">RESET</button>
            <button type="submit" id="submit" class="btn btn-warning text-white">SUBMIT</button>
        </div>
        <?= form_close(); ?>
    </div>
</body>

</html>