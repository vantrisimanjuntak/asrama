<?php
class Login extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_login');
		$this->load->library('session');
	}
	public function index()
	{
		$this->load->view('admin/tampilan_login');
	}
	public function ceklogin()
	{

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->Model_login->ambillogin($username, $password);
	}
	// function logout()
	// {
	// 	$this->session->session_destroy();
	// 	redirect();
	// }
}
