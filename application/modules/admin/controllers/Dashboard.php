<?php class Dashboard extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Admin_model');
        if (!$this->session->userdata('username')) {
            header("location:login");
        }
    }
    function index()
    {
        $data['title'] = "Portal Admin | ASAM";
        $data['total_siswa'] = $this->Admin_model->last_dataSiswa();
        $data['total_pembina'] = $this->Admin_model->last_dataPembina();
        $data['total_berita'] = $this->Admin_model->getAllData();
        $this->load->view('admin/dashboard/dashboard_v', $data);
    }
}
