<?php class Berita extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Admin_model');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url', 'file'));
        if (!$this->session->userdata('username')) {
            header("location:login");
        }
    }
    function index()
    {
        $data['title'] = 'Portal Admin | ASAM';
        $data['berita'] = $this->Admin_model->getAllNews();
        $this->load->view('admin/berita/berita_v', $data);
    }
    public function tulis_berita()
    {
        $randID = bin2hex(random_bytes(8));
        $this->Admin_model->tulis_berita($randID);
        redirect('admin/berita');
    }
    function show($link)
    {
        $data['title'] = 'Berita | Portal ASAM';
        $link = $this->uri->segment('4');
        $data['berita'] = $this->Admin_model->show_detail($link);
        $this->load->view('admin/berita/open_news', $data);
    }
    function hapus_berita($id)
    {
        //delete image
        $result = $this->db->get_where('berita', array('id' => $id));
        $rows = $result->result();
        foreach ($rows as $row) {
            unlink("image/news/" . $row->gambar);
        }
        $this->Admin_model->hapus_berita($id);
        redirect('admin/berita');
    }
}
