<?php class Pembina extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            header("location:login");
        }
        $this->load->model('admin/Admin_model');
        $this->load->helper('url');
    }
    function index()
    {
        $data['title'] = "Portal Admin | ASAM";
        $data['data_pembina'] = $this->Admin_model->getAllDataPembina();
        $data['waiting_accept'] = $this->Admin_model->waitingAcceptPembina();
        $this->load->view('admin/pembina/pembina_v', $data);
    }
    function changestatus($id)
    {
        $this->Admin_model->changeStatusPembina($id);
        redirect('admin/pembina');
    }
    function hapus($id)
    {

        $this->Admin_model->hapus_pembina($id);
    }
}
