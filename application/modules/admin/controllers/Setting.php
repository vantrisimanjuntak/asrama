<?php class Setting extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            header("location:login");
        }
        $this->load->model('Admin_model');
    }
    function index()
    {
        $data['title'] = "Portal Admin | ASAM";
        $this->load->view('admin/setting/setting_v', $data);
    }
    function open_reg()
    {
        $this->Setting_model->open_reg();
        redirect('admin/setting');
    }
    function open_reg_pembina()
    {
        $this->Admin_model->open_reg_pembina();
    }
}
