<?php class Siswa extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            header("location:login");
        }
        $this->load->model('admin/Admin_model');
        $this->load->helper('url');
    }
    function index()
    {
        $data['title'] = 'Portal Admin | ASAM';
        $data['data_siswa'] = $this->Admin_model->getAllDataSiswa();
        $data['berita'] = $this->load->view('admin/siswa/siswa_v', $data);
    }
    function update($nisn)
    {
        $this->Siswa_model->update_siswa($nisn);
        redirect('admin/siswa');
    }
    function hapus($id)
    {
        $result = $this->db->get_where('siswa', array('nisn' => $id));
        $rows = $result->result();
        foreach ($rows as $row) {
            unlink("image/profile/siswa_photo/" . $row->pas_foto);
        }
        $this->Admin_model->hapus_siswa($id);
        redirect('admin/siswa');
    }
}
