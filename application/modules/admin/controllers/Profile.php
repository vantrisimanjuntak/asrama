<?php class Profile extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Profile_m');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url', 'file'));
        if (!$this->session->userdata("username")) {
            header("location:login");
        }
    }
    function index()
    {
        $data['title'] = "Portal Admin | ASAM";
        $data['profile'] = $this->Profile_m->getProfile();
        $this->load->view('admin/profile_admin/profile_admin_v', $data);
    }
    function update()
    {
        $this->Profile_m->updateProfile();
        redirect('admin/profile');
    }
}
