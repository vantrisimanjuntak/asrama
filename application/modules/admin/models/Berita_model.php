<?php class Berita_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	function getAllNews()
	{
		$this->db->from('pembina a');
		$this->db->join('berita b', 'a.id = b.author');
		$this->db->order_by('tanggal_posting', 'DESC');
		return $this->db->get()->result_array();
		return $this->db->get('berita')->result_array();
	}
	function getAllData()
	{

		$query  = $this->db->get('berita');
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return 0;
		}
	}
	function tulis_berita()
	{

		$judul_berita = $this->input->post('judul_berita');
		$link = $this->input->post('link');
		$isi_berita = $this->input->post('isi_berita');
		$gambar = $this->_uploadImage();
		date_default_timezone_set('Asia/Jakarta');
		$tanggal_posting = date('Y-m-d H:i:s');
		$author = $this->session->userdata('id');


		$data = array(

			'judul_berita' => $judul_berita,
			'link' => $link,
			'isi_berita' => $isi_berita,
			'gambar' => $gambar,
			'tanggal_posting' => $tanggal_posting,
			'author' => $author,
		);
		$this->db->insert('berita', $data);
	}
	function hapus_berita($id)
	{
		//delete image 
		$this->db->where("id", $id);
		return $this->db->delete('berita');
	}

	private function _uploadImage()
	{
		log_message("error", print_r($_FILES, true));
		$config['upload_path'] = './image/news';
		$config['allowed_types'] = 'gif|jpeg|jpg|png';
		$config['max_size'] = '2048';
		$config['max_width'] = '0';
		$config['max_height'] = '0';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload()) {
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload_form', $error);
		} else {
			return $this->upload->data('file_name');
		}
	}
	// function getDataById($id)
	// {
	// 	$this->db->where('id', $id);
	// 	return $this->db->get('berita');
	// }

	function show_detail($id)
	{
		$this->db->from('login a');
		$this->db->join('berita b', 'a.id = b.author');
		$this->db->where('link', $id);
		$query = $this->db->get('', $id);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			redirect('error');
		}
	}
	function edit_berita($id)
	{
		$this->db->get('berita');
	}
	function update_news($id)
	{
		$judul_berita = $this->input->post('judul_berita');
		$isi_berita = $this->input->post('isi_berita');
		$tanggal_posting = $this->input->post('tanggal_posting');

		$data = array(
			'judul_berita' => $judul_berita,
			'isi_berita' => $isi_berita,
		);
		$this->db->where('id', $id);
		$this->db->update('berita', $data);
	}
	function author()
	{
		$this->db->from('login a');
		$this->db->join('berita b', 'a.id = b.author');
		return $this->db->get('berita')->result_array();
		// return $this->db->get('berita')->result_array();
	}
}
