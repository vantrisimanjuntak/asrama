<?php

/**
 * 
 */
class Siswa_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	function getAllData()
	{
		$query = $this->db->get('siswa');
		return $query->result_array();
	}
	function input_data()
	{
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$pas_foto = $this->_uploadImage();

		$data = array(
			'id' => $id,
			'nama' => $nama,
			'alamat' => $alamat,
			'jenis_kelamin' => $jenis_kelamin,
			'tanggal_lahir' => $tanggal_lahir,
			'tempat_lahir' => $tempat_lahir,
			'pas_foto' => $pas_foto,
		);
		$this->db->insert('siswa', $data);
	}
	function update_siswa($nisn)
	{
		// $nisn = $this->input->post('nisn');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$nama_ayah = $this->input->post('nama_ayah');
		$nama_ibu = $this->input->post('nama_ibu');
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$data = array(
			'nama' => $nama,
			'alamat' => $alamat,
			'jenis_kelamin' => $jenis_kelamin,
			'tanggal_lahir' => $tanggal_lahir,
			'tempat_lahir' => $tempat_lahir,
			'nama_ayah' => $nama_ayah,
			'nama_ibu' => $nama_ibu,
			'email' => $email,
			'password' => $password,
		);
		$this->db->update('siswa', $data, array('nisn' => $nisn));
	}
	function hapus_siswa($id)
	{
		$this->db->where("id", $id);
		$this->db->delete('siswa');
	}
	function last_data()
	{
		$query = $this->db->get('siswa');
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		} else {
			return 0;
		}
	}
	private function _uploadImage()
	{
		$config['upload_path'] = './image/profile/siswa_photo';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '1000';
		$config['max_height'] = '768';

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('pas_foto')) {
			return $this->upload->data('file_name');
		}
	}
}
