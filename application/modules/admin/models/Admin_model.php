<?php class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    // Function for Berita
    function getAllNews()
    {
        $this->db->from('login a');
        $this->db->join('berita b', 'a.id = b.author');
        $this->db->order_by('tanggal_posting', 'DESC');
        return $this->db->get()->result_array();
        return $this->db->get('berita')->result_array();
    }
    function getAllData()
    {
        $query  = $this->db->get('berita');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function tulis_berita($randID)
    {
        $judul_berita = $this->input->post('judul_berita');
        $link = $this->input->post('link');
        $isi_berita = $this->input->post('isi_berita');
        $gambar = $this->_uploadImage();
        date_default_timezone_set('Asia/Jakarta');
        $tanggal_posting = date('Y-m-d H:i:s');
        $author = $this->session->userdata('id');

        $data = array(
            'id' => $randID,
            'judul_berita' => $judul_berita,
            'link' => $link,
            'isi_berita' => $isi_berita,
            'gambar' => $gambar,
            'tanggal_posting' => $tanggal_posting,
            'author' => $author,
        );
        $this->db->insert('berita', $data);
    }
    private function _uploadImage()
    {
        $config['upload_path'] = './image/news';
        $config['allowed_types'] = 'gif|jpeg|jpg|png';
        // $config['max_size'] = '2048';
        // $config['max_width'] = '0';
        // $config['max_height'] = '0';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('upload_form', $error);
        } else {
            $config['file_name'] = time();
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = FALSE;
            $config['quality'] = '100%';
            $config['width'] = 600;
            $config['height'] = 400;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            return $this->upload->data('file_name');
        }
    }
    function hapus_berita($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete('berita');
    }
    function show_detail($id)
    {
        $this->db->from('login a');
        $this->db->join('berita b', 'a.id = b.author');
        $this->db->where('link', $id);
        $query = $this->db->get('', $id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            echo "ERROR";
        }
    }
    function edit_berita($id)
    {
        $this->db->get('berita');
    }
    function update_news($id)
    {
        $judul_berita = $this->input->post('judul_berita');
        $isi_berita = $this->input->post('isi_berita');
        $tanggal_posting = $this->input->post('tanggal_posting');

        $data = array(
            'judul_berita' => $judul_berita,
            'isi_berita' => $isi_berita,
        );
        $this->db->where('id', $id);
        $this->db->update('berita', $data);
    }
    function author()
    {
        $this->db->from('login a');
        $this->db->join('berita b', 'a.id = b.author');
        return $this->db->get('berita')->result_array();
    }
    // End Function


    // Function for Pembina
    function getAllDataPembina()
    {
        $this->db->from('gender a');
        $this->db->join('pembina b', 'a.kd_gender = b.jenis_kelamin');
        $active = 'Aktif';
        $this->db->where('status', $active);
        return $this->db->get()->result_array();
    }
    function waitingAcceptPembina()
    {
        $this->db->from('gender a');
        $this->db->join('pembina b', 'a.kd_gender = b.jenis_kelamin');
        $waiting = 'Menunggu';
        $this->db->where('status', $waiting);
        return $this->db->get()->result_array();
    }
    function changeStatusPembina($id)
    {
        $status = 'Aktif';
        $this->db->set('status', $status);
        $this->db->where('id_pembina', $id);
        $this->db->update('pembina');
    }
    function hapus_pembina($id)
    {
        $result = $this->db->get_where('id_pembina', array('id' => $id))->result_array();
        foreach ($result as $row) {
            unlink("image/profile/pembina_photo" . $row->pas_foto);
        }
        $this->db->where('id_pembina', $id);
        $this->db->delete('pembina');
    }
    function last_dataPembina()
    {
        $active = 'Aktif';
        $this->db->select('status');
        $this->db->from('pembina');
        $this->db->where('status', $active);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
    }
    // End Function


    // Function for Profile Admin
    function getProfile()
    {
        return $this->db->get('login');
    }
    function updateProfile()
    {
        $nama = $this->input->post('nama');
        $password = md5($this->input->post('password'));
        $email = $this->input->post('email');
        $pas_foto = $this->_uploadImageAdmin;

        $data = array(
            'nama' => $nama,
            'password' => $password,
            'email' => $email,
            'pas_foto' => $pas_foto,
        );
        $this->db->update('login', $data, array('id' => $this->session->userdata('id')));
    }
    private function _uploadImageAdmin()
    {
        $config['upload_path'] = './image/profile';
        $config['allowed_types'] = 'gif/jpg/jpeg/png';
        $config['max_size'] = '5000';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('pas_foto')) {
            return $this->upload->data('file_name');
        }
    }
    // End Function


    // Function for Setting
    function open_reg()
    {
        $data = array(
            'id' => substr(sha1(time()), 0, 8),
            'tahun_ajaran' => $this->input->post('tahun_ajaran'),
            'mulai' => $this->input->post('start'),
            'berakhir' => $this->input->post('end'),
        );
        $this->db->insert('pendaftaran', $data);
    }
    // End Function 

    // Function for status

    // End Function


    // Function for Siswa
    function getAllDataSiswa()
    {
        return $this->db->get('siswa')->result_array();
    }
    function last_dataSiswa()
    {
        $query = $this->db->get('siswa');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function hapus_siswa($id)
    {
        $this->db->where('nisn', $id);
        $this->db->delete('siswa');
    }
    // End Function
}
