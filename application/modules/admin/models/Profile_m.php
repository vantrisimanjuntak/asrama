<?php class Profile_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function getProfile()
    {
        $query = $this->db->get('login');
        return $query->result_array();
    }
    function updateProfile()
    {
        $nama = $this->input->POST('nama');
        $password = md5($this->input->POST('password'));
        $email = $this->input->POST('email');
        $posisi = $this->input->POST('posisi');
        $pas_foto = $this->_uploadImage();

        $data = array(
            'nama' => $nama,
            'password' => $password,
            'email' => $email,
            'pas_foto' => $pas_foto,
        );
        $this->db->update('login', $data, array('id' => $this->session->userdata('id')));
    }
    private function _uploadImage()
    {
        $config['upload_path'] = './image/profile';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '5000';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('pas_foto')) {
            return $this->upload->data('file_name');
        }
    }
}
