<?php 
	/**
	 * 
	 */
	class Pembina_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();

		}
		function getAllData()
		{
			$query = $this->db->get('pembina');
			return $query->result_array();
		}
		function input_data()
		{

			
			// $data = 
			// ["id" => $this->input->post('id'),
			// "no_ktp" =>$this->input->post('no_ktp'),
			// "nama" => $this->input->post('nama'),
			// "alamat" => $this->input->post('alamat'),
			// "jenis_kelamin" => $this->input->post('jenis_kelamin'),
			// "tanggal_lahir" => $this->input->post('tanggal_lahir'),
			// "tempat_lahir" => $this->input->post('tempat_lahir'),
			// "pas_foto" => $this->_uploadImage(),
			// "posisi" => $this->input->post('posisi') ];
			// $this->db->insert('data_pembina', $data);
		}
		function hapus_pembina($id)
		{
			$this->db->where("id", $id);
			$this->db->delete('pembina');
		}
		function last_data()
		{
			$query  = $this->db->get('pembina');
			if ($query -> num_rows() > 0) {
				return $query->num_rows();
			}
			else 
			{
				return 0;
			}
		}
		private function _uploadImage()
		{
			$config['upload_path'] = './pict/profiles';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size']='1000';
			$config['max_height']='768';

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('pas_foto')) {
				return $this->upload->data('file_name');
			}
		}
	}
?>