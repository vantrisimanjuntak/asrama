<?php class Setting_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function open_reg()
    {

        $data = array(
            'id' => substr(sha1(time()), 0, 8),
            'tahun_ajaran' => $this->input->post('tahun_ajaran'),
            'mulai' => $this->input->post('start'),
            'berakhir' => $this->input->post('end'),
        );
        $this->db->insert('pendaftaran', $data);
    }
}
