<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png') ?>">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/template/admin_template/vendor/fontawesome-free/css/all.min.css') ?>">
    <link href="<?= base_url('assets/template/admin_template/css/sb-admin-2.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/template/admin_template/vendor/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">
    <!-- Bootstrap Toggle Button CSS -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Sweetalert 2 CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/sweetalert/package/dist/sweetalert2.min.css'); ?>">
    <title><?= $title ?></title>
    <style>
        #overlay {
            background: #ffffff;
            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }
    </style>
    <style>
        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            display: none;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input.default:checked+.slider {
            background-color: #444;
        }

        input.primary:checked+.slider {
            background-color: #2196F3;
        }

        input.success:checked+.slider {
            background-color: #8bc34a;
        }

        input.info:checked+.slider {
            background-color: #3de0f5;
        }

        input.warning:checked+.slider {
            background-color: #FFC107;
        }

        input.danger:checked+.slider {
            background-color: #f44336;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
</head>

<body id="page-top" onload="myFunction()">
    <div id="overlay">
        <img src="<?= base_url('image/35.gif'); ?>" alt="Loading" /><br />
        Loading...
    </div>
    <div id="wrapper">
        <?php
        $this->load->view('admin/setting/setting_sidebar');
        $this->load->view('admin/setting/setting_content');
        ?>
    </div>
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/template/admin_template/js/sb-admin-2.min.js') ?>"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url('assets/template/admin_template/vendor/chart.js/Chart.min.js') ?>"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-area-demo.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-pie-demo.js') ?>"></script>

    <!-- Custom scripts for all pages-->


    <!-- Page level plugins -->
    <script src="<?= base_url('assets/template/admin_template/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "scrollX": true,
            });
        });
    </script>
    <script>
        var preloader = document.getElementById('overlay');

        function myFunction() {
            preloader.style.display = 'none';
        }
    </script>

    <!-- Bootstrap Toggle Button JS -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/template/admin_template/js/demo/datatables-demo.js') ?>"></script>
    <!-- Sweetalert 2 JS -->
    <script src="<?= base_url('assets/sweetalert/package/dist/sweetalert2.min.js') ?>" type="text/javascript"></script>
    <script>
        $('#tahunajaran, #mulai, #berakhir, #btnSubmit').attr('Disabled', true);
        $('#active, #deactive').hide();
        $('#myCheck').click(function() {
            Swal.fire({
                title: 'Apakah Anda ingin membuka pendaftaran Pembina?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Buka',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Pendaftaran dibuka',
                        '',
                        'success'
                    )
                } else {
                    alert('Hello');
                }
            })
        });

        // if ($('#desc').text() == 'AKTIF') {
        //     $('#desc').css('color', "red");
        //     $('#desc').text('TIDAK AKTIF');
        //     $('#tahunajaran, #mulai, #berakhir, #btnSubmit').attr('Disabled', true);
        // } else {
        //     $('#desc').text("AKTIF");
        //     $('#desc').css('color', "blue");
        //     $('#tahunajaran, #mulai, #berakhir, #btnSubmit').attr('Disabled', false);
        // }
    </script>
</body>

</html>