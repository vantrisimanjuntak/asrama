<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Topbar -->
        <?php $this->load->view('notif'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
            </div> -->

            <!-- Content Row -->
            <div class="row">
                <div class="col-lg-3">
                    <div class="card">
                        <img class="card-img-top" src="<?= base_url('image/ruang belajar2.jpg') ?>" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-center" style="color: black">Jadwal Pendaftaran</h5>
                            <p class="card-text">Atur Jadwal Pendaftaran</p>
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Atur Jadwal</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <img src="<?= base_url('image/pembayaran.jpeg'); ?>" class="card-img-top" height="178px;" alt="" srcset="">
                        <div class="card-body">
                            <h5 class="card-title text-center" style="color: black">Pembayaran</h5>
                            <p class="card-text">Atur Jadwal Pembayaran</p>
                            <a href="#jadwalpembayaran" class="btn btn-primary" data-toggle="modal" data-target="#jadwalpembayaran">Atur Jadwal</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">

                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- Modal For Jadwal Pendaftaran -->
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Jadwal Pendaftaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Status</label>
                            <div class="col-sm-4">
                                <label class="switch ">
                                    <input type="checkbox" class="warning">
                                    <span class="slider" id="myCheck"></span>
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <div id="desc"></div>
                            </div>
                        </div>
                        <?= form_open('admin/setting/open_reg'); ?>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Tahun Ajaran</label>
                            <div class="col-sm-10">
                                <!-- <?php $ta = date('Y'); ?> -->
                                <input type="text" class="form-control" id="tahunajaran" name="tahun_ajaran">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Mulai</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="mulai" name="start">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Berakhir</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="berakhir" name="end">
                            </div>
                        </div>
                        <button type="submit" id="btnSubmit" class="btn btn-warning">Submit</button>
                        <?php  ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal For Jadwal Pendaftaran -->

        <!-- Modal For Pembayaran -->
        <div class="modal fade" id="jadwalpembayaran" tabindex="-1" role="dialog" aria-labelledby="jadwalpembayaran" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST">
                            <div class="form-group row">
                                <label for="recipient-name" class="col-sm-4 col-form-label">Pembayaran terakhir</label>
                                <div class="col-sm-8 ">
                                    <select name="" class="form-control" id="">
                                        <option value="">-- PILIH TANGGAL --</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Atur</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal For Pembayaran -->
    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; <?= date('Y') ?> Asrama St. Albertus Magnus Aekkanopan. All Right Reserved</span>
            </div>
            <div class="copyright text-center my-auto" style="padding-top: 5px;">
                Powered by Admin
            </div>
        </div>
    </footer>
    <!-- End of Footer -->
</div>