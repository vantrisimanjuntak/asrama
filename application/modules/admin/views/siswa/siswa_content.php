<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Topbar -->
        <?php $this->load->view('notif'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div> -->

            <!-- Content Row -->


            <!-- Content Row -->

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Siswa</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NISN</th>
                                    <th>Nama</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Alamat</th>
                                    <th>Pas Foto</th>
                                    <th>Nama Ayah</th>
                                    <th>Nama Ibu</th>
                                    <!-- <th>E-mail</th> -->
                                    <!-- <th>Password</th> -->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($data_siswa as $ds) {
                                ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td>
                                            <a href="<?= base_url('admin/siswa?id=' . $ds['nisn']); ?>"><?= $ds['nisn']; ?></a></td>
                                        <td><?= $ds['nama']; ?></td>
                                        <td><?= $ds['tempat_lahir']; ?></td>
                                        <td><?= $ds['tanggal_lahir']; ?></td>
                                        <td><?= $ds['jenis_kelamin']; ?></td>
                                        <td><?= $ds['alamat']; ?></td>
                                        <td> <img src="<?= base_url('image/profile/siswa_photo/' . $ds['pas_foto']) ?>" alt="" class="img-fluid" srcset=""></td>
                                        <td><?= $ds['nama_ayah']; ?></td>
                                        <td><?= $ds['nama_ibu']; ?></td>
                                        <td>
                                            <button type="button" class="btn btn-warning" name="" data-toggle="modal" data-target="#modalDS"><i class="far fa-edit"></i></button>
                                            <a href="<?= base_url('admin/siswa/hapus/' . $ds['nisn']) ?>">
                                                <button type="button" class="btn btn-danger" name=""><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <!-- Content Row -->
            <!-- Modal -->

            <!-- End Modal -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; <?= date('Y') ?> Asrama St. Albertus Magnus Aekkanopan. All Right Reserved</span>
            </div>
            <div class="copyright text-center my-auto" style="padding-top: 5px;">
                Powered by Admin
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>