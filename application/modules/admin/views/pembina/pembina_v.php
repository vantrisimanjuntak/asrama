<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png') ?>">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/template/admin_template/vendor/fontawesome-free/css/all.min.css') ?>">
    <link href="<?= base_url('assets/template/admin_template/css/sb-admin-2.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/template/admin_template/vendor/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">
    <!-- SweetAlert2 CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/sweetalert/package/dist/sweetalert2.min.css') ?>">
    <title><?= $title ?></title>
</head>

<body id="page-top">
    <div id="wrapper">
        <?php
        $this->load->view('admin/pembina/pembina_sidebar');
        $this->load->view('admin/pembina/pembina_content');
        ?>
    </div>
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/template/admin_template/js/sb-admin-2.min.js') ?>"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url('assets/template/admin_template/vendor/chart.js/Chart.min.js') ?>"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-area-demo.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-pie-demo.js') ?>"></script>

    <!-- Custom scripts for all pages-->


    <!-- Page level plugins -->
    <script src="<?= base_url('assets/template/admin_template/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>
    <script>
        $(document).ready(function() {
            $('.nowrap').DataTable({
                "scrollX": true,
            });
        });
    </script>
    <script src="<?= base_url('assets/admin-js.js'); ?>" type="text/javascript"></script>
    <!-- SweetAlert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript" src="<?= base_url('assets/sweetalert/package/dist/sweetalert2.all.min.js'); ?>"></script>
    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/template/admin_template/js/demo/datatables-demo.js') ?>"></script>
</body>

</html>