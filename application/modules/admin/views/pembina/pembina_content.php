<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Topbar -->
        <?php $this->load->view('notif'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div> -->

            <!-- Content Row -->


            <!-- Content Row -->

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Pembina</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Pas Foto</th>
                                    <th>E-mail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($data_pembina as $dp) {
                                ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $dp['id_pembina']; ?></td>
                                        <td><?= $dp['nama']; ?></td>
                                        <td><?= $dp['alamat']; ?></td>
                                        <td><?= $dp['tempat_lahir']; ?></td>
                                        <td><?= date("d-m-Y", strtotime($dp['tanggal_lahir'])); ?></td>
                                        <td><?= $dp['gender']; ?></td>
                                        <td>
                                            <img src="<?= base_url('image/profile/pembina_photo/' . $dp['pas_foto']); ?>" alt="" class="img-fluid" srcset="">
                                        </td>
                                        <td><?= $dp['email']; ?></td>
                                        <td>
                                            <button type="button" class="btn btn-warning" name="edit">Edit</button>
                                            <a href="<?= base_url('admin/pembina/hapus/' . $dp['id_pembina']) ?>">
                                                <button type="button" class="btn btn-danger" name="hapus">Hapus</button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Menunggu Konfirmasi</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Pas Foto</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($waiting_accept as $row) {
                                ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $row['id_pembina']; ?></td>
                                        <td><?= $row['nama']; ?></td>
                                        <td><?= $row['alamat']; ?></td>
                                        <td><?= $row['tempat_lahir']; ?></td>
                                        <td><?= date("d-m-Y", strtotime($row['tanggal_lahir'])); ?></td>
                                        <td><?= $row['gender']; ?></td>
                                        <td>
                                            <img src="<?= base_url('image/profile/pembina_photo/' . $row['pas_foto']); ?>" alt="" class="img-fluid">
                                        </td>
                                        <td>
                                            <?php if ($row['status'] == "Menunggu") {
                                            ?>
                                                <h6 class="text-warning font-weight-bold"><?= $row['status']; ?></h6>
                                            <?php } elseif ($dp['status'] == "Aktif") {
                                            ?>
                                                <h6 class="text-primary font-weight-bold"><?= $row['status']; ?></h6>
                                            <?php } else {
                                            ?>
                                                <h6 class="text-danger font-weight-bold"><?= $row['status']; ?></h6>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger reject" name="rejected">Tolak</button>
                                            <button type="button" class="btn btn-primary accept" name="accept">Terima</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Tambah Pembina</h6>
                </div>
                <div class="card-body">
                    <?= form_open_multipart('admin/addpembina'); ?>
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">No. KTP</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="" id="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-labe">Alamat</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-labe">Jenis Kelamin</label>
                            <div class="col-sm-8">
                                <select name="" id="" class="form-control">
                                    <option value="">--PILIH JENIS KELAMIN--</option>
                                    <option value="">Pria</option>
                                    <option value="">Wanita</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-labe">Tempat Lahir</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="" id="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-labe">Tanggal Lahir</label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control" name="" id="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-labe">Pas Foto</label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="" id="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-labe">Email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="" id="">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <button type="button" class="btn btn-warning float-right" id="tambahpembina">Tambah</button>
                    </div>
                </div>
            </div>
            <!-- Content Row -->


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; <?= date('Y') ?> Asrama St. Albertus Magnus Aekkanopan. All Right Reserved</span>
            </div>
            <div class="copyright text-center my-auto" style="padding-top: 5px;">
                Powered by Admin
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>