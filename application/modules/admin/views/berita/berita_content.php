<style>
    .swal2-popup {
        font-size: 1.0rem !important;
    }
</style>
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Topbar -->
        <?php $this->load->view('notif'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div> -->

            <!-- List Table Berita -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h5 class="m-0 font-weight-bold text-primary">Berita </h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered display nowrap" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Judul</th>
                                    <th>Isi</th>
                                    <th>Gambar</th>
                                    <th>Tanggal Posting</th>
                                    <th>Link</th>
                                    <th>Author</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($berita as $row) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $row['id'] ?></td>
                                        <td><?= $row['judul_berita']; ?></td>
                                        <td><?= substr($row['isi_berita'], 0, 180); ?></td>
                                        <td><img src="<?= base_url('image/news/' . $row['gambar']) ?>" alt="" style="width: 140px; height:100px;" srcset=""></td>
                                        <td><?= date("d-m-Y", strtotime($row['tanggal_posting'])); ?></td>
                                        <td>
                                            <a href="<?= base_url('berita/show_detail/' . $row['link']) ?>"><?= $row['link']; ?></a></td>
                                        <td><?= $row['nama']; ?></td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 ">
                                                    <a href="<?= base_url('admin/berita/show/' . $row['link']); ?>">
                                                        <button type="button" class="btn btn-success mt-2" data-toggle="tooltip" title="Buka link"><i class="fas fa-external-link-square-alt" style="color:white"></i></button>
                                                    </a>
                                                </div>
                                                <div class="col-sm-12 col-md-12">
                                                    <a href="">
                                                        <button type="button" class="btn btn-warning mt-2" data-toggle="tooltip" title="Edit berita"><i class="fas fa-edit"></i></button>
                                                    </a>
                                                </div>
                                                <div class="col-sm-12 col-md-12 ">
                                                    <a href="<?= base_url('admin/berita/hapus_berita/' . $row['id']); ?>" class="text-center">
                                                        <button type="button" class="btn btn-danger mt-2" data-toggle="tooltip" title="Hapus berita"><i class="fas fa-trash-alt"></i></button>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End List Table Berita -->


            <div class="card mb-4" style="margin-top:12px;">
                <div class="card-header">
                    <i class="fas fa-newspaper"></i>&nbsp;Tulis Berita
                </div>
                <div class="card-body">
                    <form action="<?= base_url('admin/berita/tulis_berita'); ?>" enctype="multipart/form-data" method="POST">
                        <!-- Judul Berita -->
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label" style="color:black">Judul Berita</label>
                            <div class="col-sm-10">
                                <input type="text" name="judul_berita" id="judul" class="form-control" autocomplete="off" onkeyup="createslug()">
                            </div>
                        </div>
                        <!-- Ending Judul Berita -->
                        <!-- Link Berita -->
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label" style="color:black">Link</label>
                            <div class="col-sm-8">
                                <input type="text" readonly name="link" id="link" class="form-control">
                            </div>
                        </div>
                        <!-- Ending Link Berita -->
                        <!-- Isi Berita -->
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label color" style="color:black">Isi Berita</label>
                            <textarea id="summernote" name="isi_berita" class="col-sm-10 form-control"></textarea>
                            <script>
                                $('#summernote').summernote({
                                    placeholder: 'Tulis berita',
                                    tabsize: 27,
                                    height: 400
                                });
                            </script>
                            <!-- <div id="summernote" class="form-control" name="isi_berita"></div> -->
                            <!-- <div id="summernote"></div> -->
                        </div>
                        <!-- Ending Isi Berita -->
                        <!-- Foto/Gambar -->
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label" style="color:black">Gambar</label>
                            <div class="col-sm-8">
                                <input type="file" accept="image/*" required name="userfile" class="form-control-file">
                            </div>
                        </div>
                        <!-- Ending Foto/Gambar -->
                        <button type="submit" class="btn btn-success float-right">Posting</button>
                    </form>
                </div>
            </div>

            <!-- Content Row -->


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; <?= date('Y') ?> Asrama St. Albertus Magnus Aekkanopan. All Right Reserved</span>
            </div>
            <div class="copyright text-center my-auto" style="padding-top: 5px;">
                Powered by Admin
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>