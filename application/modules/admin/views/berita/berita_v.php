<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png') ?>">

    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/template/admin_template/vendor/fontawesome-free/css/all.min.css') ?>">
    <link href="<?= base_url('assets/template/admin_template/css/sb-admin-2.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/template/admin_template/vendor/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <!-- SweetAlert -->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/sweetalert/package/dist/sweetalert2.min.css'); ?>">
    <!-- For Summernote -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="<?= base_url('assets/bootstrap4/js/bootstrap.min.js') ?>"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    <title><?= $title ?></title>
    <style>
        #overlay {
            background: #ffffff;
            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }
    </style>
</head>

<body id="page-top" onload="myFunction()">
    <div id="overlay">
        <img src="<?= base_url('image/35.gif'); ?>" alt="Loading" /><br />
        Loading...
    </div>
    <div id="wrapper">
        <?php
        $this->load->view('admin/berita/berita_sidebar');
        $this->load->view('admin/berita/berita_content');
        ?>
    </div>
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/template/admin_template/js/sb-admin-2.min.js') ?>"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url('assets/template/admin_template/vendor/chart.js/Chart.min.js') ?>"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-area-demo.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-pie-demo.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    <!-- Collapsible -->


    <!-- Page level plugins -->
    <script src="<?= base_url('assets/template/admin_template/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                scrollX: true,
                scrollY: '50vh',
                scrollCollapse: true,
                paging: false,
            });
        });
    </script>
    <script>
        var preloader = document.getElementById('overlay');

        function myFunction() {
            preloader.style.display = 'none';
        }
    </script>
    <script>
        function createslug() {
            var judul = $('#judul').val();
            $('#link').val(slugify(judul));
        }

        function slugify(text) {
            return text.toString().toLowerCase()
                .replace(/\s+/g, '-') // Replace spaces with -
                .replace(/[^\w\-]+/g, '') // Remove all non-word chars
                .replace(/\-\-+/g, '-') // Replace multiple - with single -
                .replace(/^-+/, '') // Trim - from start of text
                .replace(/-+$/, ''); // Trim - from end of text
        }
    </script>


    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/template/admin_template/js/demo/datatables-demo.js') ?>"></script>
    <!-- SweetAlert JS -->
    <script type="text/javascript" src="<?= base_url('assets/sweetalert/package/dist/sweetalert2.all.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</body>

</html>