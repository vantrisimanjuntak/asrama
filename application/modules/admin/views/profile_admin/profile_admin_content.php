<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Topbar -->
        <?php $this->load->view('notif'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Profile Admin</h6>
                    </div>

                    <!-- Card Body -->
                    <div class="card-body">
                        <form action="<?= base_url('admin/profile/update') ?>" method="POST" enctype="multipart/form-data">
                            <div class="container">
                                <?php foreach ($profile as $dp) {
                                    ?>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label text-left">ID</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="id" disabled value="<?= $dp['id']; ?>" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label text-left">Nama</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="nama" value="<?= $dp['nama']; ?>" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label text-left">Email</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="email" value="<?= $dp['email']; ?>" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label text-left">Username</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="username" readonly value="<?= $dp['username']; ?>" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label text-left">Password</label>
                                            <div class="col-sm-8">
                                                <input type="password" name="password" value="<?php md5($dp['password']); ?>" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-5 control-label text-left">Konfirmasi Password</label>
                                            <div class="col-sm-8">
                                                <input type="password" name="conf_password" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label text-left">Foto Profil</label>
                                            <div class="col-sm-8">
                                                <input type="file" name="pas_foto" class="form-control-file border" id="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label text-left">Posisi</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="posisi" disabled value="<?= $dp['posisi']; ?>" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <img src="<?= base_url('image/profile/' . $dp['pas_foto']); ?>" class="img-fluid" width="200px;" alt="">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <button type="submit" class="btn btn-success float-right">Update</button>
                            <button type="reset" class="btn btn-info float-right" style="margin-right: 15px;">Reset</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; <?= date('Y') ?> Asrama St. Albertus Magnus Aekkanopan. All Right Reserved</span>
            </div>
            <div class="copyright text-center my-auto" style="padding-top: 5px;">
                Powered by Admin
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>