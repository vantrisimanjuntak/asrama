<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= base_url('image/LogoBaru.png') ?>">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/template/admin_template/vendor/fontawesome-free/css/all.min.css') ?>">
    <link href="<?= base_url('assets/template/admin_template/css/sb-admin-2.min.css') ?>" rel="stylesheet">
    <title><?= $title ?></title>
</head>

<body id="page-top">
    <div id="wrapper">
        <?php
        $this->load->view('admin/profile_admin/profile_admin_sidebar');
        $this->load->view('admin/profile_admin/profile_admin_content'); ?>

    </div>
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/template/admin_template/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/template/admin_template/js/sb-admin-2.min.js') ?>"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url('assets/template/admin_template/vendor/chart.js/Chart.min.js') ?>"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-area-demo.js') ?>"></script>
    <script src="<?= base_url('assets/template/admin_template/js/demo/chart-pie-demo.js') ?>"></script>

</body>

</html>