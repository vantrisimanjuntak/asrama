<?php if (!defined('BASEPATH'))
	exit('No direct script acces allowed');
class Model_login extends CI_Model
{

	public function ambillogin($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('pembina');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$sess = array(
					'username' => $row->username,
					'nama' => $row->nama,
					'pas_foto' => $row->pas_foto,
					'id' => $row->id,
				);
				$this->session->set_userdata($sess);
			}
			redirect('admin/dashboard');
		} else {
			$this->session->set_flashdata('info', 'Maaf, Username atau Password Salah');
			redirect('admin/login');
		}
	}
}
