$('.tombol-hapus').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Apakah anda yakin menghapus berita?',
        text: "Anda tidak akan bisa mengembalikannya lagi",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Hapus'
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                10000
            );
        }
        document.location.href = href;
    });
});

//konfirmasi pendaftaran pembina baru
$('#tambahpembina').on('click', function () {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        }
    })
});

//accept Pembina
$('.accept').click(function () {
    Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Anda tidak bisa mengembalikannya lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Terima Konfirmasi',
        cancelButtonText: 'Batal',
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Konfirmasi Berhasil',
                'Status Pembina berubah menjadi Aktif',
                'success'
            )
        }
    })
});

//reject Pembina
$('.reject').click(function () {
    Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Anda tidak bisa mengembalikannya lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Tolak Konfirmasi',
        cancelButtonText: 'Batal',
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Pembina ditolak',
                '',
                'success'
            )
        }
    })
});