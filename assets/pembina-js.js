//Pembayaran
$('.terima').on('click', function () {
    Swal.fire({
        title: 'Apakah Anda Yakin?',
        text: "Transaksi in tidak bisa dibatalkan!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Lanjutkan',
        cancelButtonText: 'Batalkan',
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Konfirmasi Berhasil!',
                'Konfirmasi Pembayaran berhasil dilakukan',
                'success'
            );
        }
    });
});


$('.batal').on('click', function () {
    Swal.fire({
        title: 'Error!',
        text: 'Do you want to continue',
        icon: 'error',
        confirmButtonText: 'Cool'
    })
});