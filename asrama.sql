-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 10, 2020 at 09:04 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asrama`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(100) NOT NULL,
  `judul_berita` varchar(100) NOT NULL,
  `isi_berita` text NOT NULL,
  `gambar` varchar(1000) NOT NULL,
  `tanggal_posting` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `link` varchar(100) NOT NULL,
  `author` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul_berita`, `isi_berita`, `gambar`, `tanggal_posting`, `link`, `author`) VALUES
(15, 'dsdfsdf', '<p>sdfdsfdsfsdfsf</p>', 'Screenshot_from_2020-02-25_01-25-34.png', '2020-03-30 20:17:14', 'dsdfsdf', 1),
(16, 'Selamat Datang di Website Asrama St. Albertus Magnus Aek Kanopan', '<p>Selamat Datang di Website Asrama St. Albertus Magnus Aek Kanopan. Untuk informasi lebih lanjut silahkan hubungi kontak yang tertera<br></p>', 'LogoBaru1.png', '2020-04-06 16:03:04', 'selamat-datang-di-website-asrama-st-albertus-magnus-aek-kanopan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kepengurusan`
--

CREATE TABLE `kepengurusan` (
  `id` int(4) NOT NULL,
  `pemilik` varchar(100) NOT NULL,
  `stakeholders` varchar(100) NOT NULL,
  `penanggung_jawab` varchar(100) NOT NULL,
  `ketua` varchar(100) NOT NULL,
  `wakil_ketua` varchar(100) NOT NULL,
  `bendahara` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pas_foto` varchar(50) NOT NULL,
  `posisi` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `nama`, `email`, `pas_foto`, `posisi`, `username`, `password`) VALUES
(1, 'Vantri Simanjuntak', 'vantri_antonius@yahoo.co.id', 'logo1.png', 'Admin', 'admin', '123123');

-- --------------------------------------------------------

--
-- Table structure for table `log_login_pembina`
--

CREATE TABLE `log_login_pembina` (
  `id_log` int(5) NOT NULL,
  `pembina_id` int(11) NOT NULL,
  `waktu_login` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log_login_pembina`
--

INSERT INTO `log_login_pembina` (`id_log`, `pembina_id`, `waktu_login`) VALUES
(52, 16030081, '2020-04-05 19:50:52'),
(53, 16030081, '2020-04-05 19:57:19'),
(54, 16030081, '2020-04-05 19:57:26'),
(55, 16030081, '2020-04-05 20:14:51'),
(56, 16030081, '2020-04-05 20:33:08'),
(57, 16030081, '2020-04-05 20:35:56'),
(58, 16030081, '2020-04-05 21:20:19'),
(59, 16030081, '2020-04-06 15:26:47'),
(60, 16030081, '2020-04-06 15:37:57'),
(61, 16030081, '2020-04-06 15:43:17'),
(62, 16030081, '2020-04-06 15:44:56'),
(63, 16030081, '2020-04-06 17:49:13'),
(64, 16030081, '2020-04-06 19:43:51'),
(65, 16030081, '2020-04-07 17:51:23'),
(66, 16030081, '2020-04-07 18:56:12'),
(67, 16030081, '2020-04-08 10:05:40'),
(68, 16030081, '2020-04-08 15:50:46'),
(69, 16030081, '2020-04-08 16:30:31'),
(70, 16030081, '2020-04-08 19:03:04'),
(71, 16030081, '2020-04-09 18:04:43'),
(72, 16030081, '2020-04-10 18:11:00'),
(73, 16030081, '2020-04-10 18:34:26');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `No` int(5) NOT NULL,
  `Id_Pegawai` varchar(10) NOT NULL,
  `NIK` int(10) NOT NULL,
  `No_KK` int(10) NOT NULL,
  `Nama Lengkap` varchar(20) NOT NULL,
  `Tempat Lahir` int(10) NOT NULL,
  `Tanggal Lahir` int(10) NOT NULL,
  `Jenis Kelamin` enum('Laki-Laki','Perempuan') NOT NULL,
  `Jabatan` varchar(10) NOT NULL,
  `No_Laporan` varchar(10) NOT NULL,
  `User_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_siswa`
--

CREATE TABLE `pembayaran_siswa` (
  `kode_bayar` int(100) NOT NULL,
  `nisn` int(20) NOT NULL,
  `waktu` int(11) NOT NULL,
  `jumlah_pembayaran` int(100) NOT NULL,
  `penerima` int(10) NOT NULL,
  `status` enum('Pending','Belum Bayar','Sudah Bayar','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pembina`
--

CREATE TABLE `pembina` (
  `id_pembina` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `tempat_lahir` text NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `pas_foto` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembina`
--

INSERT INTO `pembina` (`id_pembina`, `nama`, `alamat`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `pas_foto`, `username`, `password`, `email`, `status`) VALUES
(12, 'sas', 'asa', 'asa', '2020-02-22', '', '', 'asa', 'facafa74878399363f0b336bec9baceccc71b517', '', 'Aktif'),
(16030080, 'Gas', 'Gas', 'Gas', '2020-02-22', '', '', 'gas', 'facafa74878399363f0b336bec9baceccc71b517', 'gas', 'Aktif'),
(16030081, 'Vantri Antonius Simanjuntak', 'Kisaran', 'Kisaran', '2020-02-22', 'Pria', '16030081.png', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'vantri_antonius@yahoo.co.id', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id` varchar(8) NOT NULL,
  `tahun_ajaran` year(4) NOT NULL,
  `mulai` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `berakhir` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id`, `tahun_ajaran`, `mulai`, `berakhir`) VALUES
('b8e0dac7', 2021, '2020-12-31 17:00:00', '2021-01-31 17:00:00'),
('ce911de9', 2020, '2020-04-08 17:00:00', '2020-04-15 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nisn` int(6) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` text NOT NULL,
  `jenis_kelamin` enum('Pria','Wanita') NOT NULL,
  `alamat` text NOT NULL,
  `pas_foto` varchar(1000) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nisn`, `nama`, `tanggal_lahir`, `tempat_lahir`, `jenis_kelamin`, `alamat`, `pas_foto`, `nama_ayah`, `nama_ibu`) VALUES
(102, 'Andre Mangara', '2020-03-13', 'dfgdfg', 'Wanita', 'dfgfdg', 'LogoBaru.png', '', ''),
(852, 'Ger', '2020-04-09', 'Gds', 'Pria', 'Ger', 'Screenshot_from_2020-02-25_01-25-34.png', '', ''),
(12422, 'Dora Explorer', '2020-03-21', 'Lembang', 'Wanita', 'Bogor', 'IMG-20180928-WA0029.jpg', 'sdfds', 'sdadasfsa'),
(1012313, 'Vantri Antonius Simanjuntak', '2020-03-12', 'Kisaran', 'Pria', 'Kisaran', 'Foto.jpg', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author` (`author`);

--
-- Indexes for table `kepengurusan`
--
ALTER TABLE `kepengurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_login_pembina`
--
ALTER TABLE `log_login_pembina`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `pembina_id` (`pembina_id`);

--
-- Indexes for table `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  ADD PRIMARY KEY (`kode_bayar`),
  ADD KEY `nisn` (`nisn`),
  ADD KEY `penerima` (`penerima`);

--
-- Indexes for table `pembina`
--
ALTER TABLE `pembina`
  ADD PRIMARY KEY (`id_pembina`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nisn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_login_pembina`
--
ALTER TABLE `log_login_pembina`
  MODIFY `id_log` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  MODIFY `kode_bayar` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_ibfk_1` FOREIGN KEY (`author`) REFERENCES `login` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_login_pembina`
--
ALTER TABLE `log_login_pembina`
  ADD CONSTRAINT `log_login_pembina_ibfk_1` FOREIGN KEY (`pembina_id`) REFERENCES `pembina` (`id_pembina`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  ADD CONSTRAINT `pembayaran_siswa_ibfk_2` FOREIGN KEY (`penerima`) REFERENCES `pembina` (`id_pembina`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `pembayaran_siswa_ibfk_3` FOREIGN KEY (`nisn`) REFERENCES `siswa` (`nisn`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
